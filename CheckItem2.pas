unit CheckItem2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables;

type
  TfrmCheckItem2 = class(TfrmMaster)
    ListBox1: TListBox;
    Button1: TButton;
    Query1: TQuery;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private �錾 }
  public
    { Public �錾 }
  end;

var
  frmCheckItem2: TfrmCheckItem2;

implementation
uses
Inter;
{$R *.DFM}

//�`�F�b�N�J�n�{�^��
procedure TfrmCheckItem2.Button1Click(Sender: TObject);
var
  sSql,sSID,sTC,sIC1,sIC2,sCMP1, sCMP2 : String;
  sTCa,sIC1a,sIC2a : String;
  i,j:Integer;
begin
  ListBox1.Items.Clear;
  sSql := 'SELECT * FROM ' + CtblMItem2;
  sSql := sSql + ' order by TokuisakiCode,Code1,Code2';
  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 0;
    j := 0;
    sSID  := FieldByName('SID').AsString;
    sTC  := FieldByName('TokuisakiCode').AsString;
    sIC1 := FieldByName('Code1').AsString;
    sIC2 := FieldByName('Code2').AsString;
    sCMP1 := sTc+'-'+sIC1+'-'+sIC2;
    Next;
    while not EOF do begin
      sTCa  := FieldByName('TokuisakiCode').AsString;
      sIC1a := FieldByName('Code1').AsString;
      sIC2a := FieldByName('Code2').AsString;
      sCMP2 := sTca+'-'+sIC1a+'-'+sIC2a;
      sSID  := FieldByName('SID').AsString;
      if sCMP1=sCMP2 then begin
        ListBox1.Items.Add(sSID + '-' + sCMP2);
        ListBox1.Update;
        sCMP1 := sCMP2;
        i := i + 1;
        j := j + 1;
        Next;
      end else begin
        sCMP1 := sCMP2;
        i := i + 1;
{        if i > 6000 then begin
          Exit;
        end;
}
        Next;
      end;
      SB1.SimpleText := sCMP1 + ' ������=' + IntToStr(i) + '���@2�d����=' + IntToStr(j) + '��';
      SB1.Update;
    end;//of while
    Close;
  end;
end;

//2�d�f�[�^�̍폜
procedure TfrmCheckItem2.Button2Click(Sender: TObject);
var
  sSql,sSID : String;
  i : Integer;
begin

for i:=0 to ListBox1.Items.Count-1 do begin
  sSID := copy(ListBox1.Items[i], 1,Pos('-',ListBox1.Items[i])-1);
  sSql := 'DELETE FROM ' + CtblMItem2;
  sSql := sSql + ' where SID='+sSID;
  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;
  SB1.SimpleText := IntToStr(i+1) + '���폜���܂���';
  SB1.Update;
 end;
end;

end.
