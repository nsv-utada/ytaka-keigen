inherited frmCopyItem: TfrmCopyItem
  Left = 359
  Top = 116
  Width = 513
  Height = 339
  Caption = 'frmCopyItem'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 505
    inherited Label1: TLabel
      Width = 186
      Caption = #21830#21697#12487#12540#12479#12467#12500#12540#30011#38754
    end
    inherited Label2: TLabel
      Left = 278
    end
  end
  inherited Panel2: TPanel
    Top = 246
    Width = 505
    inherited BitBtn1: TBitBtn
      Left = 368
    end
    inherited BitBtn2: TBitBtn
      Left = 208
      Caption = #12467#12500#12540#38283#22987
      OnClick = BitBtn2Click
    end
  end
  inherited Panel3: TPanel
    Width = 505
    Height = 205
    object Label3: TLabel
      Left = 30
      Top = 12
      Width = 81
      Height = 12
      Caption = #24471#24847#20808#12467#12540#12489#65297
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 30
      Top = 37
      Width = 24
      Height = 12
      Caption = #12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 30
      Top = 62
      Width = 52
      Height = 12
      Caption = #24471#24847#20808#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 30
      Top = 100
      Width = 117
      Height = 13
      Caption = #21830#21697#32207#12510#12473#12479#12540#12363#12425
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 212
      Top = 100
      Width = 101
      Height = 13
      Caption = #24471#24847#20808#12467#12540#12489#12363#12425
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 38
      Top = 128
      Width = 56
      Height = 13
      Caption = #20385#26684#21306#20998
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clFuchsia
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 226
      Top = 128
      Width = 76
      Height = 13
      Caption = #24471#24847#20808#12467#12540#12489
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clFuchsia
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CBTokuisakiCode1: TComboBox
      Left = 126
      Top = 8
      Width = 97
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnKeyDown = CBTokuisakiCode1KeyDown
    end
    object CBTokuisakiNameYomi: TComboBox
      Left = 126
      Top = 32
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
      OnKeyDown = CBTokuisakiNameYomiKeyDown
    end
    object CBTokuisakiName: TComboBox
      Left = 126
      Top = 57
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 2
      OnExit = CBTokuisakiNameExit
    end
    object RB420: TRadioButton
      Left = 78
      Top = 142
      Width = 113
      Height = 17
      Caption = 'S(420)'
      Checked = True
      TabOrder = 3
      TabStop = True
    end
    object RB450: TRadioButton
      Left = 78
      Top = 160
      Width = 113
      Height = 17
      Caption = 'L(450)'
      TabOrder = 4
    end
    object CBTokuisakiCode2: TComboBox
      Left = 252
      Top = 146
      Width = 97
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 5
      OnKeyDown = CBTokuisakiCode1KeyDown
    end
  end
  inherited SB1: TStatusBar
    Top = 287
    Width = 505
    SimplePanel = True
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 236
    Top = 19
  end
end
