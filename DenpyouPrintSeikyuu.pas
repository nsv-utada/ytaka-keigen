unit DenpyouPrintSeikyuu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, jpeg, StdCtrls, QRPrntr, Mask;

type
  TfrmDenpyouPrintSeikyuu = class(TForm)
    qrpDenpyouPrint: TQuickRep;
    PageHeaderBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRSysData1: TQRSysData;
    qryDenpyouPrint: TQuery;
    qryForDetailBand: TQuery;
    DetailBand1: TQRBand;
    QRShape6: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    qdtDetail0: TQRDBText;
    qdtDetail1: TQRDBText;
    qdtDetail2: TQRDBText;
    qdtDetail3: TQRDBText;
    qdtDetail4: TQRDBText;
    qdtDetail4_1: TQRDBText;
    qdtDetail6: TQRDBText;
    qdtDetail13: TQRDBText;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    qdtGoukei: TQRExpr;
    qdtDenpyouCode: TQRExpr;
    qdtNouhinDate: TQRExpr;
    lblNouhinDate: TQRLabel;
    lblTokuisakiCode1: TQRLabel;
    line1: TQRShape;
    lblTokuisakiCode2: TQRLabel;
    lblTitle: TQRLabel;
    QRShape9: TQRShape;
    lblTokuisakiName: TQRLabel;
    lblSama: TQRLabel;
    lblNenngou: TQRLabel;
    lblYear: TQRLabel;
    QRShape10: TQRShape;
    lblNenn: TQRLabel;
    lblMonth: TQRLabel;
    lblTuki: TQRLabel;
    lblDate: TQRLabel;
    lblHi: TQRLabel;
    lblHinmei: TQRLabel;
    lblSuuryou: TQRLabel;
    lblTanni: TQRLabel;
    lblTannka: TQRLabel;
    lblKinngaku: TQRLabel;
    QRShape1: TQRShape;
    lblChainCode: TQRLabel;
    lblMonth2: TQRLabel;
    QRShape2: TQRShape;
    QRLabel14: TQRLabel;
    QRExprMemo1: TQRExprMemo;
    SummaryBand1: TQRBand;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel6: TQRLabel;
    QRShape5: TQRShape;
    QRLabel3: TQRLabel;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape11: TQRShape;
    QRLabel5: TQRLabel;
    QRShape12: TQRShape;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape13: TQRShape;
    QRShape17: TQRShape;
    qryDenpyou10Print: TQuery;
    qryDenpyouSumPrint: TQuery;
    qryDenpyou8Print: TQuery;
    QRShape18: TQRShape;
    qdtDetailTax: TQRDBText;
    QRLabel10: TQRLabel;
    qdtShoukei: TQRExpr;
    qdtTax: TQRExpr;
    qdtShoukei10: TQRExpr;
    qdtTax10: TQRExpr;
    qdtSoukei: TQRExpr;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    Function MakeDenpyouSeikyuu(strTokuisakiCode1: String; strStartDate: String; strEndDate: String): Integer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmDenpyouPrintSeikyuu  : TfrmDenpyouPrintSeikyuu;
  strDenpyouBanngou       : String;

implementation

uses Inter, Denpyou2,MTokuisaki, HKLib;

{$R *.DFM}

procedure TfrmDenpyouPrintSeikyuu.FormCreate(Sender: TObject);
var
 strTokuisakiCode1 : String;
 strStartDate      : String;
 strEndDate        : String;

begin

 // 得意先コード，StartDate，EndDateをローカル変数へコピー
 strTokuisakiCode1 := GTokuisakiCode1;
 strStartDate      := GStartDate;
 strEndDate        := GEndDate;

//
// 印刷を行う．
//

 // 伝票作成 (対象伝票が１つも無い561場合は処理をしない)
 if MakeDenpyouSeikyuu(strTokuisakiCode1, strStartDate, strEndDate) =0 then
   begin
     // 使用するプリンタをデフォルトのプリンタに設定（実はこれなくてもいいかも)
     qrpDenpyouPrint.PrinterSettings.PrinterIndex := -1;
     // プリント
     qrpDenpyouPrint.Print;
   end;
                                    
   //2002.07.31
  Close;

end;


procedure TfrmDenpyouPrintSeikyuu.FormDestroy(Sender: TObject);
begin

 // リソースの開放
 qryDenpyouPrint.Close;
 qryForDetailBand.Close;

end;


Function TfrmDenpyouPrintSeikyuu.MakeDenpyouSeikyuu(strTokuisakiCode1: String;
                                          strStartDate: String; strEndDate: String): Integer;
var
 sSql                   : String;
 strTokuisakiCode2      : String;
 strTokuisakiName       : String;
 strChainCode           : String;
 wY,wM,wD               : Word;
 sYear,sMonth,sDay      : String;
 iTax, iSoukei          : integer;
 douTax                 : Double;
 newTax                 : Double;
 ssYear,ssMonth,ssDay   : String;

 dCTax,dCTax10,dShoukei,dShouhizei8,dShouhizei10 : Double;
 sCTax,sCTax10 : String;
 curShouhizei           : Currency;
 
 iMakeTax  : integer;
begin

 // 得意先Code1, 得意先Code2，得意先名 取得
 sSql := 'SELECT ChainCode,TokuisakiCode1, TokuisakiCode2, TokuisakiName FROM ' + CtblMTokuisaki;
 sSql := sSql + ' WHERE TokuisakiCode1 = ' + '''' + strTokuisakiCode1 + '''';
  with qryDenpyouPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    strTokuisakiCode2 := FieldbyName('TokuisakiCode2').AsString;
    //2002.08.30
    //strTokuisakiName  := FieldbyName('TokuisakiName').AsString;
	  strTokuisakiName := MTokuisaki.GetTokuisakiName(strTokuisakiCode1);

    strChainCode      := FieldbyName('ChainCode').AsString;
    Close;
  end;

 // 得意先Code1, 得意先Code2，得意先名 セット （得意先コードは4桁にしている)
 lblTokuisakiCode1.Caption := FormatFloat('0000', StrToInt(strTokuisakiCode1));
 lblTokuisakiCode2.Caption := strTokuisakiCode2;
 lblChainCode.Caption      := strChainCode;
 lblTokuisakiName.Caption  := strTokuisakiName;

 // 伝票日付セット
 DecodeDate(StrToDate(strEndDate), wY, wM, wD);
// sYear  := IntToStr(wY-1988); // 平成へ変換 20190306 uta
 sYear  := IntToStr(wY); // 西暦へ変換 20190306 uta
 sMonth := IntToStr(wM);
 sDay   := IntToStr(wD);

 lblYear.Caption  := sYear;
 lblMonth.Caption := sMonth;
 lblMonth2.Caption := sMonth;
 lblDate.Caption  := sDay;

 // 明細行セット
 sSql := 'SELECT DD.DenpyouCode DenpyouCode, DD.NouhinDate NouhinDate, D.UriageKingaku UriageKingaku, ';
 sSql := sSql + 'DD.No No, MI.Name Name, MI.Kikaku Kikaku, MI.Irisuu Irisuu, DD.Suuryou Suuryou, ';
 sSql := sSql + 'MI.Tanni Tanni, DD.Tannka Tannka, DD.Shoukei Shoukei, DD.Shouhizei Shouhizei FROM ';
 sSql := sSql + CtblTDenpyouDetail + ' DD INNER JOIN ' + CtblTDenpyou;
 sSql := sSql + ' D ON DD.DenpyouCode = D.DenpyouCode INNER JOIN ' + CtblMItem;
 sSql := sSql + ' MI ON DD.Code1 = MI.Code1 AND DD.Code2 = MI.Code2';
 sSql := sSql + ' WHERE DD.TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
 sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';

 //sSql := sSql + ' ORDER BY DD.DenpyouCode, DD.No';

 //Added by H.kubota 2002.07.15
 sSql := sSql + ' ORDER BY DD.NouhinDate, DD.DenpyouCode, DD.No';

  with qryForDetailBand do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;

    // もしレコードが無かったら処理を抜ける．
    if RecordCount =0 then begin
      Close;
      Result := 1;
      Exit;
    end;

    QRGroup1.Expression       := 'qryForDetailBand.DenpyouCode';
    qdtDenpyouCode.Expression := 'qryForDetailBand.DenpyouCode';
    qdtNouhinDate.Expression  := 'qryForDetailBand.NouhinDate';
    qdtGoukei.Expression      := 'qryForDetailBand.UriageKingaku';
    qdtDetail0.DataField    := 'No';
    qdtDetail1.DataField    := 'Name';
    qdtDetail2.DataField    := 'Kikaku';
    qdtDetail3.DataField    := 'Irisuu';
    qdtDetail4.DataField    := 'Suuryou';
    qdtDetailTax.DataField    := 'Shouhizei';
    qdtDetail4_1.DataField  := 'Tanni';
    qdtDetail6.DataField    := 'Tannka';
    qdtDetail13.DataField   := 'Shoukei';

  end;

 // 合計セット
 // labelではMaskプロパティを使えないので \#,##と指定して値を整形できない．
 // しょうがなく，DBTextを使用している．
 {
 sSql := 'SELECT SUM(Shoukei) Soukei, SUM(Shoukei)*0.05 Tax, SUM(Shoukei)*1.05 Zeikomi FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
 sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';
 sSql := sSql + ' GROUP BY TokuisakiCode1';
 }
//thuyptt 20190814
// sSql := 'SELECT';
// sSql := sSql + ' SUM(Shoukei) as Soukei,';
// sSql := sSql + ' SUM(CASE WHEN NouhinDate < ''' + CsChangeTaxDate + ''' then Shoukei * ' + FloatToStr(CsOldTaxRate) + ' else Shoukei * ' +  FloatToStr(CsTaxRate) + ' end)  as Tax,';
// sSql := sSql + ' SUM(Shoukei) + (SUM(CASE WHEN NouhinDate < ''' + CsChangeTaxDate + ''' then Shoukei * ' + FloatToStr(CsOldTaxRate) + ' else Shoukei * ' +  FloatToStr(CsTaxRate) + ' end)) as Zeikomi';
// sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
// sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
// sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
// sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';
// sSql := sSql + ' GROUP BY TokuisakiCode1';



 // 伝票日付セット
 DecodeDate(StrToDate(GEndDate), wY, wM, wD);
 ssYear  := IntToStr(wY); // 西暦へ変換 20190306 uta
 ssMonth := IntToStr(wM);
 ssDay   := IntToStr(wD);

 douTax := CsTaxRate * 100;
 newTax := CsNewTaxRate * 100;

{
 sSql := 'SELECT SUM(UriageKingaku) Tax FROM ' + CtblTDenpyou;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND Memo = ' + '''' + CsCTAX + '''';
 sSql := sSql + ' AND DATEPART(yy, Gatsubun) = ' + sYear;
 sSql := sSql + ' AND DATEPART(mm, Gatsubun) = ' + sMonth;
 sSql := sSql + ' AND Bikou =' + '''' + floattostr(douTax) + '%''';

  with qryDenpyou8Print do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
  end;

  qdtTax.DataField := 'Tax';

 sSql := 'SELECT SUM(UriageKingaku) Tax10 FROM ' + CtblTDenpyou;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND Memo = ' + '''' + CsCTAX + '''';
 sSql := sSql + ' AND DATEPART(yy, Gatsubun) = ' + sYear;
 sSql := sSql + ' AND DATEPART(mm, Gatsubun) = ' + sMonth;
 sSql := sSql + ' AND Bikou =' + '''' + floattostr(newTax) + '%''';

  with qryDenpyou10Print do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
  end;

  qdtTax10.DataField := 'Tax10';

 sSql := 'SELECT SUM(tab.Shouhizei8) AS Shouhizei8, SUM(tab.Shouhizei10) AS Shouhizei10 FROM';
 sSql := sSql + ' (';
 sSql := sSql + ' SELECT';
 sSql := sSql + ' SUM(CASE Shouhizei WHEN ' + floattostr(douTax) + ' THEN Shoukei ELSE 0 END) AS Shouhizei8, 0 AS Shouhizei10' ;
 sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
 sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';
 sSql := sSql + ' GROUP BY TokuisakiCode1, Shouhizei';
 sSql := sSql + ' UNION ';
 sSql := sSql + ' SELECT';
 sSql := sSql + ' 0 AS Shouhizei8, SUM(CASE Shouhizei WHEN ' + floattostr(newTax) + ' THEN Shoukei ELSE 0 END) AS Shouhizei10';
 sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
 sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';
 sSql := sSql + ' GROUP BY TokuisakiCode1, Shouhizei';
 sSql := sSql + ' ) AS tab';

  with qryDenpyouPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
  end;

  qdtShoukei.DataField    := 'Shouhizei8';
  qdtShoukei10.DataField  := 'Shouhizei10';

}



 sSql := 'SELECT SUM(tab.Shouhizei8) AS Shouhizei8, SUM(tab.Tax8) AS tax8, SUM(tab.Shouhizei10) AS Shouhizei10, SUM(tab.Tax10) AS tax10 FROM';
 sSql := sSql + ' (';
 sSql := sSql + ' SELECT';
 sSql := sSql + ' SUM(CASE Shouhizei WHEN ' + floattostr(douTax) + ' THEN Shoukei ELSE 0 END) AS Shouhizei8, 0 Tax8, 0 AS Shouhizei10, 0 Tax10' ;
 sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
 sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';
 sSql := sSql + ' GROUP BY TokuisakiCode1, Shouhizei';
 
 sSql := sSql + ' UNION ';
 sSql := sSql + ' SELECT';
 sSql := sSql + ' 0 AS Shouhizei8, SUM(Shoukei) * 0.08 Tax8, 0 AS Shouhizei10, 0 AS Tax10 FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
 sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';  
 sSql := sSql + ' AND Shouhizei = ' + '' + floattostr(douTax) + '';

 sSql := sSql + ' UNION ';
 sSql := sSql + ' SELECT';
 sSql := sSql + ' 0 AS Shouhizei8, 0 Tax8, SUM(CASE Shouhizei WHEN ' + floattostr(newTax) + ' THEN Shoukei ELSE 0 END) AS Shouhizei10, 0 Tax10';
 sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
 sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';
 sSql := sSql + ' GROUP BY TokuisakiCode1, Shouhizei';

 sSql := sSql + ' UNION ';
 sSql := sSql + ' SELECT';
 sSql := sSql + ' 0 AS Shouhizei8, 0 Tax8, 0 AS Shouhizei10, SUM(Shoukei) * 0.10 AS Tax10 FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + strTokuisakiCode1 + '''';
 sSql := sSql + ' AND NouhinDate >= ' + '''' + strStartDate + '''';
 sSql := sSql + ' AND NouhinDate <= ' + '''' + strEndDate + '''';   
 sSql := sSql + ' AND Shouhizei = ' + '' + floattostr(newTax) + '';
 sSql := sSql + ' ) AS tab';
 

  with qryDenpyouSumPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;

    dCTax   := FieldByName('tax8').AsFloat;
    dCTax10 := FieldByName('tax10').AsFloat;
    dShouhizei8   := FieldByName('Shouhizei8').AsFloat;
    dShouhizei10 := FieldByName('Shouhizei10').AsFloat;
  end;  

  //qdtSoukei.DataField  := 'Shouhizei';

  qdtShoukei.Expression      := CurrtoStr(dShouhizei8);
  qdtShoukei10.Expression    := CurrtoStr(dShouhizei10);

  dCTax := Round5(dCTax);
  dCTax10 := Round5(dCTax10);  

  qdtTax.Expression   := CurrtoStr(dCTax);
  qdtTax10.Expression := CurrtoStr(dCTax10);

  sCTax   := Real2Str(dCTax, 0, 0);
  sCTax10 := Real2Str(dCTax10, 0, 0);
  curShouhizei := StrToInt(sCTax) +  StrToInt(sCTax10);

  dShoukei := curShouhizei + dShouhizei8 + dShouhizei10;

  qdtSoukei.Expression   := CurrtoStr(dShoukei);  

  Result := 0;

end;


procedure TfrmDenpyouPrintSeikyuu.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 //2002.07.31
	Action := caFree;

end;

end.
