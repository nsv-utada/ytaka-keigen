unit GennkinnKaishuu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables;

type
  TfrmGennkinnKaishuu = class(TfrmMaster)
    pnlTokuisakiName: TPanel;
    pnlDennpyouCode: TPanel;
    pnlNouhinDate: TPanel;
    dtpNouhinDate: TDateTimePicker;
    cmbTokuisakiName: TComboBox;
    cmbDennpyouBanngou: TComboBox;
    cmdKanniKennsaku: TButton;
    cmdDennyouBanngouKennsaku: TButton;
    Label4: TLabel;
    Label3: TLabel;
    cmdDennpyouHyouji: TButton;
    Label5: TLabel;
    pnlBar: TPanel;
    pnlNouhinDate2: TPanel;
    pnlDennpyouBanngou2: TPanel;
    pnlTokuisakiName2: TPanel;
    pnlKinngaku2: TPanel;
    pnlMemo: TPanel;
    edbNouhinDate2: TEdit;
    edbDennpyouBanngou2: TEdit;
    edbTokuisakiName2: TEdit;
    edbKinngaku2: TEdit;
    memMemo: TMemo;
    BitBtn3: TBitBtn;
    pnlflg: TPanel;
    edbflg: TEdit;
    qryGennkinnKaishuu: TQuery;
    Label6: TLabel;
    pnlKaishuubi: TPanel;
    dtpKaishuubi: TDateTimePicker;
    Label7: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cmbTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
    procedure cmdKanniKennsakuClick(Sender: TObject);
    procedure cmdDennyouBanngouKennsakuClick(Sender: TObject);
    procedure cmdDennpyouHyoujiClick(Sender: TObject);
    procedure cmbTokuisakiNameExit(Sender: TObject);
    procedure Clear;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure cmbDennpyouBanngouKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure dtpKaishuubiChange(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmGennkinnKaishuu: TfrmGennkinnKaishuu;

implementation

uses
  DM1,Inter, DMMaster, TokuisakiKensaku,PasswordDlg;

{$R *.DFM}

procedure TfrmGennkinnKaishuu.FormCreate(Sender: TObject);
begin

  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
 if dlgPasswordDlg.ShowModal = mrOK then begin

 end else begin
 		Exit;
 end;

  //dlgPasswordDlg.ShowModal;
  //dlgPasswordDlg.Visible := true;

//	dlgPasswordDlg.Create(Self);

 //TdlgPasswordDlg.Create;

 // Memoの内容をクリア
 memMemo.Text := '';

 // 納品日を当日に設定
 dtpNouhinDate.Date := Date();

 // 納品日を当日に設定
 dtpKaishuubi.Date := Date()-1;

end;


procedure TfrmGennkinnKaishuu.FormActivate(Sender: TObject);
begin

 	// 伝票番号ボックスへフォーカスを移す．
	cmbDennpyouBanngou.SetFocus;

  //
	if GPassWord = CPassword0 then begin
    Beep;
  end else begin
  	//ShowMessage('PassWordが違います');
    close;
  end;
  GPassWord := '';
end;


procedure TfrmGennkinnKaishuu.cmbTokuisakiNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  // 得意先名簡易検索
	if (Key=VK_F1) then
    begin
     	MakeCBTokuisakiName('TokuisakiNameYomi', cmbTokuisakiName.Text, 1);
    end;

end;


procedure TfrmGennkinnKaishuu.MakeCBTokuisakiName(
  sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  // コンボボックスクリア
  cmbTokuisakiName.Items.Clear;

	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  cmbTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	cmbTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
       FieldByName('TokuisakiNameUp').AsString + ' ' + 
       FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  cmbTokuisakiName.DroppedDown := True;

end;


procedure TfrmGennkinnKaishuu.cmdKanniKennsakuClick(Sender: TObject);
begin

  // 得意先簡易検索画面オープン
  TfrmTokuisakiKensaku.Create(Self);

end;


procedure TfrmGennkinnKaishuu.cmdDennyouBanngouKennsakuClick(
  Sender: TObject);
var
 sNouhinDate     : String;
 sTokuisakiCode1 : String;
 sSql            : String;

begin

  // コンボボックスクリア
  cmbDennpyouBanngou.Items.Clear;

  // パラメータ作成
  DateTimeToString(sNouhinDate, 'yyyy/mm/dd', dtpNouhinDate.Date);
  sTokuisakiCode1 := Copy(cmbTokuisakiName.Text, 1, (pos(',', cmbTokuisakiName.Text)-1));

try

  // コンボボックスリスト作成
	sSql := 'SELECT DISTINCT DenpyouCode FROM ' + CtblTDenpyouDetail;
  sSql := sSql + ' WHERE NouhinDate = ' + '''' + sNouhinDate + '''';
  sSql := sSql + ' AND TokuisakiCode1 = ' + '''' + sTokuisakiCode1 + '''';
  sSql := sSql + ' ORDER BY DenpyouCode ';

  cmbDennpyouBanngou.Clear;

	with qryGennkinnKaishuu do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then
      begin
        ShowMessage('該当する伝票番号は存在しません．');
        Close;
        Exit;
      end;

    while not EOF do begin
    	cmbDennpyouBanngou.Items.Add(FieldByName('DenpyouCode').AsString);
    	Next;
    end;
    Close;
  end;

  cmbDennpyouBanngou.DroppedDown := True;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


procedure TfrmGennkinnKaishuu.cmbTokuisakiNameExit(Sender: TObject);
var
 sTokuisakiCode1 : String;

begin

 // 得意先コードが50000番代でなければ現金回収対象でないのでメッセージ出力

  sTokuisakiCode1 := Copy(cmbTokuisakiName.Text, 1, (pos(',', cmbTokuisakiName.Text)-1));

 if (StrToInt(sTokuisakiCode1) < 50000) or (StrToInt(sTokuisakiCode1) >= 60000) then
   begin
     ShowMessage('この得意先は現金回収対象ではありません．得意先コードが50000番代の得意先を指定して下さい．');
     Exit;
   end;

end;


procedure TfrmGennkinnKaishuu.cmdDennpyouHyoujiClick(Sender: TObject);
var
 sDennpyouBanngou : String;
 sSql             : String;
 sKaishuuflg      : String;
 sKaishuuDate     : String;
 sTokuisakiCode1,sTokuisakiName  : String;
 sTableName, sReturnFieldName, sWhere : String;
 sMemo     : String;
begin

 //
 // 伝票の内容を表示する．
 //

 // Sql文作成・実行

 sDennpyouBanngou := cmbDennpyouBanngou.Text;

 sSql := 'SELECT * FROM ' + CtblTGennkinnKaishuu;
 sSql := sSql + ' WHERE DenpyouCode = ' + '''' + sDennpyouBanngou + '''';

	with qryGennkinnKaishuu do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then
      begin
        ShowMessage('エラー発生：この伝票番号のデータが存在しません．');
        Close;
        Exit;
      end;

    // データ取得・データセット
  	edbNouhinDate2.Text      := FieldByName('NouhinDate').AsString;
  	sKaishuuflg              := FieldByName('Kaishuuflg').AsString;
  	edbDennpyouBanngou2.Text := FieldByName('DenpyouCode').AsString;
  	edbKinngaku2.Text        := FieldByName('Goukei').AsString;
    sKaishuuDate             := FieldByName('KaishuuDate').AsString;
    sMemo                    := FieldByName('Memo').AsString;
    //added　2003.02.09
    sTokuisakiCode1          := FieldByName('TokuisakiCode1').AsString;
    //特定のテーブルから特定のフィールドデータを文字型で返す
		sTableName := CtblMTokuisaki;
    sReturnFieldName := 'TokuisakiName';
    sWhere := ' where TokuisakiCode1=' + sTokuisakiCode1;
	  sTokuisakiName := DM1.GetFieldData(sTableName, sReturnFieldName, sWhere);


    Close;

  end;

  // 得意先名セット
  //2003.02.09 変更
  //edbTokuisakiName2.Text := cmbTokuisakiName.Text;
  edbTokuisakiName2.Text := sTokuisakiName;

  //Memoの表示　2003.02.09
  memMemo.Text := sMemo;

  // 回収/未回収セット
  if sKaishuuflg = '0' then
    begin
      edbflg.Text := '未回収';
    end
  else if sKaishuuflg = '1' then
    begin
      edbflg.Text := '回収済';
    end
  else
    begin
      ShowMessage('回収/未回収フラグの設定でエラー発生');
    end;

  // 回収日セット．Null(=未回収)なら前日に，Nullでなければ(=回収済)，そのデータをセット
  if sKaishuuDate = '' then
    begin
      //200.07.29
      //dtpKaishuubi.Date := Date()-1;
    end
  else
    begin
      dtpKaishuubi.Date := StrToDateTime(sKaishuuDate);
    end;

  // 回収済ボタンへフォーカスを移す．
  BitBtn2.SetFocus;

end;


procedure TfrmGennkinnKaishuu.BitBtn2Click(Sender: TObject);
var
 sDennpyouBanngou : String;
 sShoukei         : String;
 sShouhizei       : String;
 sGoukei          : String;
 sKaishuuDate     : String;
 sMemo            : String;
 sSql             : String;

begin

try

 //
 // 回収済に設定する．
 //

  // パラメータ作成
  sDennpyouBanngou := edbDennpyouBanngou2.Text;
  sGoukei          := edbKinngaku2.Text;
  sShouhizei       := CurrToStr(Trunc(StrToCurr(sGoukei)*CsTaxRate/(1.0+CsTaxRate)));
  sShoukei         := CurrToStr(StrToCurr(sGoukei)-StrToCurr(sShouhizei));
  sKaishuuDate     := DateToStr(dtpKaishuubi.Date);
  sMemo            := memMemo.Text;

  // SQL文作成
  sSql := 'Update ' + CtblTGennkinnKaishuu;
  //sSql := sSql + ' SET Shoukei = CONVERT(money,' + '''' + sShoukei     + '''),';
  //sSql := sSql + ' Shouhizei = CONVERT(money,'   + '''' + sShouhizei   + '''),';
  sSql := sSql + ' SET Goukei = CONVERT(money,'      + '''' + sGoukei      + '''),';
  sSql := sSql + ' KaishuuDate = ' + '''' + sKaishuuDate + ''',';
  sSql := sSql + ' Kaishuuflg = '  + '''' + '1'          + ''',';
  sSql := sSql + ' memo = '        + '''' + sMemo        + '''';
  sSql := sSql + ' WHERE DenpyouCode = ' + '''' + sDennpyouBanngou + '''';

  // SQL文実行
  with qryGennkinnKaishuu do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
	  ExecSql;
    Close;
  end;

  // メッセージを出力
  ShowMessage('回収済に設定しました．');

  // 表示をクリアする．
  Clear;

  // 伝票番号ボックスへフォーカスを移す．
  cmbDennpyouBanngou.SetFocus;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


procedure TfrmGennkinnKaishuu.BitBtn3Click(Sender: TObject);
var
 sDennpyouBanngou : String;
 sKaishuuDate     : String;
 sMemo            : String;
 sSql             : String;

begin

try

 //
 // 未回収に設定する．
 //

  // パラメータ作成
  sDennpyouBanngou := edbDennpyouBanngou2.Text;
  sKaishuuDate     := DateTimeToStr(Date());
  sMemo            := memMemo.Text;

  // SQL文作成
  sSql := 'Update ' + CtblTGennkinnKaishuu;
  sSql := sSql + ' SET KaishuuDate = NULL,';
  sSql := sSql + ' Kaishuuflg = ' + '''' + '0' + ''',';
  sSql := sSql + ' memo = ' + '''' + sMemo + '''';
  sSql := sSql + ' WHERE DenpyouCode = ' + '''' + sDennpyouBanngou + '''';

  // SQL文実行
  with qryGennkinnKaishuu do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
	  ExecSql;
    Close;
  end;

  // メッセージを出力
  ShowMessage('未回収に設定しました．');

  // 表示をクリアする．
  Clear;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


procedure TfrmGennkinnKaishuu.Clear;
begin

 // 表示をクリアする
 edbNouhinDate2.Text      := '';
 edbflg.Text              := '';
 edbDennpyouBanngou2.Text := '';
 edbTokuisakiName2.Text   := '';
 edbKinngaku2.Text        := '';
 memMemo.Text             := '';

end;


procedure TfrmGennkinnKaishuu.cmbDennpyouBanngouKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  // Enterを押したら，Focusを伝票表示ボタンへフォーカスを移す．
  if Key=VK_RETURN then cmdDennpyouHyouji.SetFocus;

end;

procedure TfrmGennkinnKaishuu.dtpKaishuubiChange(Sender: TObject);
begin
  inherited;

  // 回収済ボタンへフォーカスを移す．
  BitBtn2.SetFocus;

end;

end.
