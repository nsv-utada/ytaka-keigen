unit GennkinnZanndaka;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables,ShellAPI;

type
  TfrmGennkinnZanndaka = class(TfrmMaster)
    Label3: TLabel;
    CBYyyy: TComboBox;
    Label4: TLabel;
    CBMm: TComboBox;
    Label5: TLabel;
    rdgShuturyokuTaishou: TRadioGroup;
    QueryTokuisaki: TQuery;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private 宣言 }
    Function GetTax(sDenpyouCode :String):String;
  public
    { Public 宣言 }
  end;

var
  frmGennkinnZanndaka: TfrmGennkinnZanndaka;

implementation

uses Inter, HKLib, PasswordDlg, DMMaster;

{$R *.DFM}


procedure TfrmGennkinnZanndaka.FormCreate(Sender: TObject);
var
  wYyyy, wMm, wDd      : Word;
  sYear, sMonth, sDay  : String;

begin

 // 初期設定

   // 出力対象のデフォルトを’全て’に設定
   rdgShuturyokuTaishou.ItemIndex := 0;

   // 年・月を当日の年・月に設定
   DecodeDate(Date(), wYyyy, wMm, wDd);
	 sYear  := IntToStr(wYyyy);
   sMonth := IntToStr(wMm);
   sDay   := IntToStr(wDd);

   CBYyyy.Text := sYear;
   CBMm.Text   := sMonth;

	//2002.12.28
	dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
	if dlgPasswordDlg.ShowModal = mrOK then begin

	end else begin
		Exit;
	end;

end;


procedure TfrmGennkinnZanndaka.BitBtn2Click(Sender: TObject);
var
  sLine            : String;
 	F                : TextFile;
  sSql             : String;
  sGatsubun        : String;
  sSql2            : String;

   douTax               : Double;
   newTax               : Double;

   dCTaxOrg,dCTaxNew : Double;
   sCTaxOrg,sCTaxNew : String;

begin

	// 確認メッセージ出力

  if MessageDlg('出力しますか?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
    begin
      Exit;
    end;

	//出力するファイルを作成する、すでにあれば削除する

 	AssignFile(F, CFileName_GennkinnZanndaka);
  Rewrite(F);
	CloseFile(F);

  //タイトル部分の出力

  sLine := '当日日付  ' + DateTimeToStr(Date());
  HMakeFile(CFileName_GennkinnZanndaka, sLine);

  sLine := '';
  HMakeFile(CFileName_GennkinnZanndaka, sLine);

  sLine := CBYyyy.Text + '年'+CBMm.Text +'月分の現金残高';
  HMakeFile(CFileName_GennkinnZanndaka, sLine);

  sLine := '伝票番号'+ ',' + '納品日' + ',' + '得意先コード１' + ',' + '得意先名' + ',' + '金額';
  sLine := sLine + ',' + 'コース' + ',' + '回収日' + ',' + '回収'+ ',' + 'メモ' + ',' + '8%商品分'+ ',' + '10%商品分';
  HMakeFile(CFileName_GennkinnZanndaka, sLine);

  // SQL作成

  sGatsubun := CBYyyy.Text + '/' + CBMm.Text + '/01';

	//sSql := 'SELECT G.DenpyouCode DenpyouCode, G.NouhinDate NouhinDate, G.TokuisakiCode1 TokuisakiCode1, ';
  //sSql := sSql + 'T.TokuisakiName TokuisakiName, G.Goukei Goukei, G.CourseID CourseID, ';
  //sSql := sSql + 'G.KaishuuDate KaishuuDate, G.Kaishuuflg Kaishuuflg, G.memo memo FROM ';
  //sSql := sSql + CtblTGennkinnKaishuu +' G INNER JOIN ' + CtblTDenpyou + ' D ON ';
  //sSql := sSql + ' G.DenpyouCode = D.DenpyouCode INNER JOIN ' + CtblMTokuisaki + ' T ON ';
  //sSql := sSql + ' G.TokuisakiCode1 = T.TokuisakiCode1 ';
  //sSql := sSql + 'WHERE (D.Gatsubun = ' + '''' + sGatsubun + ''')';

   douTax := CsTaxRate * 100;
   newTax := CsNewTaxRate * 100;

  sSql2 := 'SELECT D.DenpyouCode';
  sSql2 := sSql2 + ',  SUM(CASE DD.Shouhizei WHEN ' + floattostr(douTax) + ' THEN DD.Shoukei ELSE 0 END) * 1.08  AS Shouhizei8';
  sSql2 := sSql2 + ', SUM(CASE DD.Shouhizei WHEN ' + floattostr(newTax) + ' THEN DD.Shoukei ELSE 0 END) * 1.1 AS Shouhizei10';
  sSql2 := sSql2 + ' FROM ' + CtblTDenpyou + ' D';
  sSql2 := sSql2 + ' INNER JOIN ' + CtblTDenpyouDetail + ' AS DD ON ';
  sSql2 := sSql2 + ' D.DenpyouCode = DD.DenpyouCode ';
  sSql2 := sSql2 + ' WHERE (D.Gatsubun = ' + '''' + sGatsubun + ''') GROUP BY D.DenpyouCode';


	sSql := 'SELECT G.DenpyouCode DenpyouCode, G.NouhinDate NouhinDate, G.TokuisakiCode1 TokuisakiCode1, ';
  sSql := sSql + 'T.TokuisakiName TokuisakiName, G.Goukei Goukei, G.CourseID CourseID, ';
  sSql := sSql + 'tab.Shouhizei8 AS Shouhizei8, ';
  sSql := sSql + 'tab.Shouhizei10 AS Shouhizei10, ';
  sSql := sSql + 'G.KaishuuDate KaishuuDate, G.Kaishuuflg Kaishuuflg, G.memo memo FROM ';
  sSql := sSql + CtblTGennkinnKaishuu +' G INNER JOIN ' + CtblTDenpyou + ' D ON ';
  sSql := sSql + ' G.DenpyouCode = D.DenpyouCode INNER JOIN ' + CtblMTokuisaki + ' T ON ';
  sSql := sSql + ' G.TokuisakiCode1 = T.TokuisakiCode1 ';
  sSql := sSql + ' INNER JOIN (' + sSql2 + ') AS tab ON ';
  sSql := sSql + ' G.DenpyouCode = tab.DenpyouCode ';
  //sSql := sSql + 'WHERE (D.Gatsubun = ' + '''' + sGatsubun + ''')';

  if rdgShuturyokuTaishou.ItemIndex = 1 then
    begin
      sSql := sSql + ' AND (G.Kaishuuflg = 0)';
    end;
  //2002.07.19 added by H.Kubota
  sSql := sSql + ' ORDER BY G.DenpyouCode';

  // データ書き出し

try

  with QueryTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then
      begin
        ShowMessage('対象の伝票は存在しません．');
        Close;
        Exit;
      end;

    While not EOF do begin

      sLine := FieldByName('DenpyouCode').AsString;
      sLine := sLine + ',' + FieldByName('NouhinDate').AsString;
      sLine := sLine + ',' + FieldByName('TokuisakiCode1').AsString;
      sLine := sLine + ',' + FieldByName('TokuisakiName').AsString;
      sLine := sLine + ',' + FieldByName('Goukei').AsString;
      sLine := sLine + ',' + FieldByName('CourseID').AsString;
      sLine := sLine + ',' + FieldByName('KaishuuDate').AsString;
      if FieldByName('Kaishuuflg').AsString = '0' then
        begin
          sLine := sLine + ',未済';
        end
      else if FieldByName('Kaishuuflg').AsString = '1' then
        begin
          sLine := sLine + ',済';
        end
      else
        begin
          ShowMessage('回収フラグの取得にてエラー発生');
        end;
      sLine := sLine + ',' + FieldByName('memo').AsString;


      //sLine := sLine + ',' + GetTax(FieldByName('DenpyouCode').AsString);

      dCTaxOrg := FieldByName('Shouhizei8').AsFloat;
      dCTaxNew := FieldByName('Shouhizei10').AsFloat;

      sCTaxOrg := Real2Str(dCTaxOrg, 0, 0);
      sCTaxNew := Real2Str(dCTaxNew, 0, 0);

      sLine := sLine + ',' + sCTaxOrg;
      sLine := sLine + ',' + sCTaxNew;


		  HMakeFile(CFileName_GennkinnZanndaka, sLine);

    	Next;

    end;

    Close;

  end;

  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '現金残高.xls', '', SW_SHOW);

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


procedure TfrmGennkinnZanndaka.FormActivate(Sender: TObject);
begin
  inherited;
	//2002.12.28
	if GPassWord = CPassword1 then begin
    Beep;
	end else begin
	//ShowMessage('PassWordが違います');
   close;
	end;
	GPassWord := '';

end;

function TfrmGennkinnZanndaka.GetTax(sDenpyouCode: String): String;

var
  sSql2 : String;
  iShouhizei8, iShouhizei10 : Integer;

   douTax               : Double;
   newTax               : Double;
     
begin
   douTax := CsTaxRate * 100;
   newTax := CsNewTaxRate * 100;
   
  sSql2 := 'SELECT ';
  sSql2 := sSql2 + '  SUM(CASE DD.Shouhizei WHEN ' + floattostr(douTax) + ' THEN DD.Shoukei ELSE 0 END) * 1.08  AS Shouhizei8';
  sSql2 := sSql2 + ', SUM(CASE DD.Shouhizei WHEN ' + floattostr(newTax) + ' THEN DD.Shoukei ELSE 0 END) * 1.1 AS Shouhizei10';
  sSql2 := sSql2 + ' FROM ' + CtblTDenpyouDetail + ' DD';
  sSql2 := sSql2 + ' WHERE DD.DenpyouCode =  ' + sDenpyouCode;

  with frmDMMaster.QueryDenpyou do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql2);
    Open;

    iShouhizei8  := FieldByName('Shouhizei8').AsInteger;
    iShouhizei10 := FieldByName('Shouhizei10').AsInteger;

    Close;
  end;

  Result := IntToStr(iShouhizei8) + ',' + IntToStr(iShouhizei10);
end;

end.
