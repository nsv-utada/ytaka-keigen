inherited frmHacchuu: TfrmHacchuu
  Left = 408
  Top = 117
  Caption = 'frmHacchuu'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    inherited Label1: TLabel
      Width = 120
      Caption = '商品発注画面'
    end
    inherited Label2: TLabel
      Left = 238
    end
  end
  inherited Panel2: TPanel
    inherited BitBtn1: TBitBtn
      Left = 342
      Cancel = True
      Kind = bkCustom
    end
    inherited BitBtn2: TBitBtn
      Left = 244
      Width = 93
      Caption = '発注書印刷'
      Glyph.Data = {00000000}
      Kind = bkCustom
    end
  end
  inherited Panel3: TPanel
    Height = 38
    Align = alTop
    object Label3: TLabel
      Left = 24
      Top = 14
      Width = 237
      Height = 13
      Caption = '発注の必要な仕入先は赤く点滅します。'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'ＭＳ Ｐゴシック'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object SG1: TStringGrid
    Left = 0
    Top = 79
    Width = 535
    Height = 207
    Align = alClient
    TabOrder = 4
    OnDblClick = SG1DblClick
  end
  object Timer1: TTimer
    Interval = 500
    OnTimer = Timer1Timer
    Left = 176
    Top = 18
  end
end
