unit Hacchuu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids;

type
  TfrmHacchuu = class(TfrmMaster)
    Label3: TLabel;
    SG1: TStringGrid;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure SG1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
  public
    { Public 宣言 }
  end;

const
	//グリッドの列の定義

  CintNum            = 0;
  CintShiiresakiCode = 1; //仕入先コード
  CintShiiresakiName = 2;
  CintItemCount      = 3;  //現在数量

  CintEnd           = 4;   //ターミネーター


	GRowCount = 300; //行数


var
  frmHacchuu: TfrmHacchuu;

implementation
uses
	Hacchuu2;
{$R *.DFM}

procedure TfrmHacchuu.Timer1Timer(Sender: TObject);
begin
	if Label3.Font.Color = clRed then begin
		Label3.Font.Color := clNavy;
  end else begin
		Label3.Font.Color := clRed;
  end;
end;

procedure TfrmHacchuu.SG1DblClick(Sender: TObject);
begin
  TfrmHacchuu2.Create(Self);
end;

procedure TfrmHacchuu.FormCreate(Sender: TObject);
begin
  inherited;
  //
  width := 450;
  height := 450;

  //ストリンググリッドの作成
  MakeSG;

end;
procedure TfrmHacchuu.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;

    ColWidths[CintNum]:= 40;
    Cells[CintNum, 0] := 'No.';

    ColWidths[CintShiiresakiCode]:= 90;
    Cells[CintShiiresakiCode, 0] := '仕入先コード';

    ColWidths[CintShiiresakiName]:= 100;
    Cells[CintShiiresakiName, 0] := '仕入先名';


  end;
end;

end.
