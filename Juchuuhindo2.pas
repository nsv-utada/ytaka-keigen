unit Juchuuhindo2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TokuisakibetsuKakaku, Db, DBTables, ComCtrls, StdCtrls, Buttons, ExtCtrls, ShellAPI;

type
  TfrmJuchuuhindo2 = class(TfrmTokuisakibetsuKakaku)
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmJuchuuhindo2: TfrmJuchuuhindo2;

implementation
uses
	Inter, HKLib, DMMaster;

{$R *.DFM}

procedure TfrmJuchuuhindo2.BitBtn2Click(Sender: TObject);
var
	sSql, sTokuisakiCode1, sLine, sSum, sCode1,sCode2,sDateFrom, sDateTo : String;
 	F : TextFile;
  sSqlTokuisakiCode2,sMaker,sTanni,sName,sSqlCode1 : String;
  i, j : Integer;
  sSID,sSuuryou,sLastDay : String;
begin
//  inherited;
	//エラーチェック
  if (MemoTokuisakiCode.Lines[0]='') then begin
    ShowMessage('得意先コードを指定してください');
    exit;
  end;
  if (MemoItemCode.Lines[0]='') then begin
    ShowMessage('商品コードを指定してください');
    exit;
  end;
  if Gi*Gj> 10000 then begin
    ShowMessage('得意先と商品の組み合わせが多すぎます -> '+ IntToStr(Gi*Gj));
    exit;
  end;

	//確認メッセージ
  if MessageDlg('エクセルへ出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  //集計期間の作成
 	sLastDay := IntToStr(HKLib.GetGetsumatsu(
                    StrToInt(CBYyyyTo.Text),StrToInt(CBMmTo.Text))
      	        );
  sDateFrom := CBYyyyFrom.Text + '/' + CbMmFrom.Text + '/01';
  sDateTo   := CBYyyyTo.Text   + '/' + CbMmTo.Text   + '/' + sLastDay;


	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_JyuchuuHindo);
  Rewrite(F);
	CloseFile(F);

  //タイトル部分の出力
  sLine := sDateFrom + '月分から' + sDateTo + '月分までの頻度集計';
  HMakeFile(CFileName_JyuchuuHindo, sLine);

  //メーカー名,商品名,単位,得意先の頻度,・・・・
  sLine := 'メーカー名,商品名,単位,商品コード1,商品コード2';
  i := 0;
  while MemoTokuisakiCode.Lines[i] <> '' do begin
		sLine := sLine + ',' + StringReplace(MemoTokuisakiCode.Lines[i],',','-',[]);
    i := i + 1;
  end;
  HMakeFile(CFileName_JyuchuuHindo, sLine);

  //商品名でループ
  i := 0;
  while MemoItemCode.Lines[i] <> '' do begin
    sSID := Copy(MemoItemCode.Lines[i],1, Pos(',',MemoItemCode.Lines[i])-1);
		sSqlCode1 := 'Select * from tblMItem where SID=' + sSID;

    //商品の基本情報の取得
	  with Query1 do begin
  		Close;
    	Sql.Clear;
    	Sql.Add(sSqlCode1);
    	open;
   		sCode1 := FieldByName('Code1').asString;
  		sCode2 := FieldByName('Code2').asString;
     	sMaker := FieldByName('Maker').asString;
     	sTanni := FieldByName('Tanni').asString;
   		sName  := FieldByName('Name').asString;
      Close;
    end;//of with
    sLine := sMaker + ',' + sName + ',' + sTanni + ',' + sCode1 + ',' + sCode2;

    //得意先ごとの頻度の出力
    j := 0;
	  while MemoTokuisakiCode.Lines[j] <> '' do begin
    	sTokuisakiCode1 := Copy(MemoTokuisakiCode.Lines[j], 1, Pos(',', MemoTokuisakiCode.Lines[j])-1);
			sSql := 'select sSuuryou=sum("Suuryou") from tblTDenpyouDetail ';
		  sSql := sSql + ' WHERE ';
		  sSql := sSql + ' NouhinDate BETWEEN ''' + sDateFrom + '''';
		  sSql := sSql + ' AND ';
		  sSql := sSql + '''' + sDateTo + '''';
		  sSql := sSql + ' AND ';
		  sSql := sSql + ' TokuisakiCode1= ' + sTokuisakiCode1;
		  sSql := sSql + ' AND ';
		  sSql := sSql + ' Code1=''' + sCode1 + '''';
		  sSql := sSql + ' AND ';
		  sSql := sSql + ' Code2=''' + sCode2 + '''';
 		  with QueryCalc do begin
		  	Close;
		    Sql.Clear;
    		Sql.Add(sSql);
		    Open;
        sSuuryou := FieldByName('sSuuryou').asString;
        if sSuuryou = '' then begin
          sSuuryou := '0';
        end;
	      sLine := sLine + ',' + sSuuryou;
        Close;
      end;//of with
 	  	j := j + 1;
 		end;//of while
    SB1.SimpleText := sLine;
	  SB1.Update;
	  HMakeFile(CFileName_JyuchuuHindo, sLine);
    i := i + 1;
  end;

  //エクセルの起動
  // 2006.11.07   added  .\\
  //ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '.\\受注頻度一覧.xls', '', SW_SHOW);
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', '.\\受注頻度一覧.xls', '', SW_SHOW);

end;

end.
