unit JyuchuuHindo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UriageShuukei, Db, DBTables, ComCtrls, StdCtrls, Buttons, ExtCtrls, ShellAPI;

type
  TfrmJyuchuuHindo = class(TfrmUriageShuukei)
    Label6: TLabel;
    Label11: TLabel;
    EditChainCode: TEdit;
    CBTokuisakiName: TComboBox;
    Label12: TLabel;
    Label13: TLabel;
    CBName: TComboBox;
    Query1: TQuery;
    Label15: TLabel;
    CBCode1: TComboBox;
    QueryCode1: TQuery;
    procedure BitBtn2Click(Sender: TObject);
    procedure CBTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure EditChainCodeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private 宣言 }
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
		procedure MakeCBName(sKey, sWord:String; iKubun:Integer);
  public
    { Public 宣言 }
  end;

var
  frmJyuchuuHindo: TfrmJyuchuuHindo;

implementation

uses
	Inter, HKLib, DMMaster;

{$R *.DFM}

procedure TfrmJyuchuuHindo.BitBtn2Click(Sender: TObject);
var
	sSql, sTokuisakiCode1, sLine, sSum, sCode1,sCode2,sDateFrom, sDateTo : String;
 	F : TextFile;
  sSqlTokuisakiCode2,sMaker,sTanni,sName,sSqlCode1 : String;
begin
	//確認メッセージ
  if MessageDlg('受注頻度一覧を集計しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  //集計期間の作成
  sDateFrom := CBYyyyFrom.Text + '/' + CBMmFrom.Text + '/1';
  sDateTo   := CBYyyyTo.Text   + '/' + CBMmTo.Text   + '/1';

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_JyuchuuHindo);
  Rewrite(F);
	CloseFile(F);

  //タイトル部分の出力
  sLine := sDateFrom + '月分から' + sDateTo + '月分までの頻度集計';
  HMakeFile(CFileName_JyuchuuHindo, sLine);

  //得意先コード・商品コードの取得
  sTokuisakiCode1 :=Copy(CBTokuisakiName.Text, 1, (pos(',', CBTokuisakiName.Text)-1));
  sCode1 :=Copy(CBCode1.Text, 1, (pos(',', CBCode1.Text)-1));

  //商品名でループ


  //メーカー名,商品名,単位,得意先の頻度,・・・・
  sLine := 'メーカー名,商品名,単位,商品コード1,商品コード2';
  //EditChainCode得意先コードでループ
  if EditChainCode.text <> '' then begin
	  if MessageDlg('チェーンコードで出力します?',
  	  mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    	Exit;
	  end;
    sSqlTokuisakiCode2 := 'select * from tblMTokuisaki where ChainCode=';
    sSqlTokuisakiCode2 := sSqlTokuisakiCode2 + EditChainCode.text;
    sSqlTokuisakiCode2 := sSqlTokuisakiCode2 + ' and ';
    sSqlTokuisakiCode2 := sSqlTokuisakiCode2 + ' HeitenFlag = 0';
    sSqlTokuisakiCode2 := sSqlTokuisakiCode2 + ' order by TokuisakiCode1';
    with QueryTokuisaki do begin
	  	Close;
  	  Sql.Clear;
    	Sql.Add(sSqlTokuisakiCode2);
	    open;
  	  while not EOF do begin
      	sLine := sLine + ',' + FieldByName('TokuisakiCode1').AsString;
    		next;
      end;//of whilr
    end;
  end else begin
	  sLine := sLine + ',' + sTokuisakiCode1;
  end;//of if
  HMakeFile(CFileName_JyuchuuHindo, sLine);
  //ここまでタイトル作成

	SB1.SimpleText := '計算中 ';

  //受注頻度の計算
  //得意先でループ
with QueryTokuisaki do begin
  first;
  sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
  //商品コード1でループ
	sSqlCode1 := 'Select * from tblMItem where Code1=''' + sCode1 + '''';
  with QueryCode1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSqlCode1);
    open;
    while not EOF do begin
    	sCode2 := FieldByName('Code2').asString;
      sMaker := FieldByName('Maker').asString;
      sTanni := FieldByName('Tanni').asString;
    	sName  := FieldByName('Name').asString;
		  //select sum('Suuryou') where Code1='' and Code2='' and TokuisakiCode1= and
		  // NouhinDate between sDateFrom and sDateTo
			sSql := 'select sSuuryou=sum("Suuryou") from tblTDenpyouDetail ';
		  sSql := sSql + ' WHERE ';
		  sSql := sSql + ' NouhinDate BETWEEN ''' + sDateFrom + '''';
		  sSql := sSql + ' AND ';
		  sSql := sSql + '''' + sDateTo + '''';
		  sSql := sSql + ' AND ';
		  sSql := sSql + ' TokuisakiCode1= ' + sTokuisakiCode1;
		  sSql := sSql + ' AND ';
		  sSql := sSql + ' Code1=''' + sCode1 + '''';
		  sSql := sSql + ' AND ';
		  sSql := sSql + ' Code2=''' + sCode2 + '''';
		  with Query1 do begin
		  	Close;
		    Sql.Clear;
    		Sql.Add(sSql);
		    Open;
    		While not EOF do begin
          sLine := sMaker + ',' + sName + ',' + sTanni + ',' + sCode1 + ',' + sCode2;
		      sLine := sLine + ',' + FieldByName('sSuuryou').AsString;
		      SB1.SimpleText := sLine;
    		  SB1.Update;
				  HMakeFile(CFileName_JyuchuuHindo, sLine);
        	next;
        end;//of while
      end;//of with
      next;
    end;//of while
  end;//of with
  Next;
end;//of with


  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '受注頻度一覧.xls', '', SW_SHOW);

end;

procedure TfrmJyuchuuHindo.CBTokuisakiNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
    CBTokuisakiName.SetFocus;
	 	MakeCBTokuisakiName('TokuisakiNameYomi', CBTokuisakiName.Text, 1);
  end;
end;
{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmJyuchuuHindo.MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBTokuisakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  CBTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
       FieldByName('TokuisakiNameUp').AsString + ' ' +
       FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBTokuisakiName.DroppedDown := True;
end;

procedure TfrmJyuchuuHindo.CBNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
    CBName.SetFocus;
	 	MakeCBName('Yomi', CBName.Text, 1);
  end;

end;
{
	コンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmJyuchuuHindo.MakeCBName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMItem;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY Yomi ';
  CBName.Clear;
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBName.Items.Add(FieldByName('SID').AsString +
       ',' + FieldByName('Name').AsString +
       ',' + FieldByName('Irisuu').AsString +
       ',' + FieldByName('Kikaku').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBName.DroppedDown := True;

end;

procedure TfrmJyuchuuHindo.FormCreate(Sender: TObject);
begin
  inherited;
	width := 550;
  height := 300;

end;

procedure TfrmJyuchuuHindo.EditChainCodeKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  CBTokuisakiName.Text := '';
end;

end.
