object frmKakakuKoushin: TfrmKakakuKoushin
  Left = 545
  Top = 157
  Width = 861
  Height = 780
  ActiveControl = BitBtn1
  Caption = 'frmKakakuKoushin'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 12
  object TPanel
    Left = 0
    Top = 0
    Width = 853
    Height = 57
    Align = alTop
    Color = clTeal
    TabOrder = 0
    object Label2: TLabel
      Left = 266
      Top = 24
      Width = 251
      Height = 13
      AutoSize = False
      Caption = #40441#26494#23627#22770#25499#31649#29702#12471#12473#12486#12512' Ver5.00'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 10
      Width = 240
      Height = 19
      Alignment = taCenter
      Caption = #24471#24847#20808#21029#20385#26684#19968#25324#22793#26356#30011#38754
      Color = clTeal
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
  end
  object SG1: TStringGrid
    Left = 32
    Top = 312
    Width = 681
    Height = 169
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 1
    OnDblClick = SG1DblClick
    OnDrawCell = SG1DrawCell
    OnKeyPress = SG1KeyPress
    OnMouseDown = SG1MouseDown
    OnSetEditText = SG1SetEditText
    ColWidths = (
      64
      64
      64
      64
      64)
    RowHeights = (
      24
      24)
  end
  object SB2: TStatusBar
    Left = 0
    Top = 734
    Width = 853
    Height = 19
    Panels = <>
  end
  object pnlInput: TPanel
    Left = 0
    Top = 57
    Width = 853
    Height = 248
    Align = alTop
    TabOrder = 3
    object Label19: TLabel
      Left = 18
      Top = 12
      Width = 76
      Height = 12
      Caption = #21830#21697#12467#12540#12489'(1)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 18
      Top = 36
      Width = 39
      Height = 12
      Caption = #21830#21697#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel
      Left = 18
      Top = 63
      Width = 24
      Height = 12
      Caption = #12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 322
      Top = 220
      Width = 84
      Height = 13
      Caption = #22793#26356#24460#12398#20385#26684
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label32: TLabel
      Left = 280
      Top = 12
      Width = 68
      Height = 12
      Caption = #21830#21697#12467#12540#12489'(2)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
    end
    object LabelSID: TLabel
      Left = 362
      Top = 40
      Width = 4
      Height = 12
    end
    object Label22: TLabel
      Left = 376
      Top = 40
      Width = 59
      Height = 12
      Caption = #21336#20385'420 '#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel
      Left = 376
      Top = 62
      Width = 59
      Height = 12
      Caption = #21336#20385'450 '#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tanka420: TLabel
      Left = 464
      Top = 40
      Width = 7
      Height = 12
      Alignment = taRightJustify
      Caption = '0'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 480
      Top = 40
      Width = 13
      Height = 12
      Caption = #20870
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 480
      Top = 64
      Width = 13
      Height = 12
      Caption = #20870
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tanka450: TLabel
      Left = 464
      Top = 64
      Width = 7
      Height = 12
      Alignment = taRightJustify
      Caption = '0'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 160
      Top = 144
      Width = 13
      Height = 12
      Caption = #24180
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 328
      Top = 144
      Width = 13
      Height = 12
      Caption = #24180
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 22
      Top = 186
      Width = 5
      Height = 13
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 22
      Top = 146
      Width = 5
      Height = 13
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 178
      Width = 425
      Height = 33
      TabOrder = 19
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 82
      Width = 425
      Height = 95
      TabOrder = 13
    end
    object CBCode1: TComboBox
      Left = 102
      Top = 6
      Width = 163
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnExit = CBCode1Exit
      OnKeyDown = CBCode1KeyDown
      Items.Strings = (
        '01,'#26989#21209#29992#39135#26448
        '02,'#35519#21619#26009#35519#21619#39135#21697
        '03,'#39321#36763#26009
        '04,'#12499#12531#12289#32566#35440#39006
        '05,'#12467#12540#12498#12540#12289#21931#33590#26448#26009
        '06,'#20083#35069#21697#65288#12481#12540#12474#39006#65289
        '07,'#39135#29992#27833
        '08,'#31881#35069#21697
        '09,'#12418#12385
        '10,'#12473#12497#40634
        '11,'#40634
        '12,'#31859
        '13,'#20013#33775#26448#26009
        '14,'#21508#31278#12472#12517#12540#12473
        '15,'#12362#33590
        '16,'#27703#12471#12525#12483#12503
        '17,'#28460#29289#24803#33756
        '18,'#12362#36890#12375#29289
        '19,'#39135#32905#21152#24037#21697
        '20,'#28023#29987#29645#21619
        '21,'#20919#20941#39135#21697#39770#20171#39006
        '22,'#20919#20941#39135#21697#36786#29987#29289
        '23,'#20919#20941#39135#21697#19968#27425#21152#24037#21697
        '24,'#20919#20941#39135#21697#28857#24515#39006
        '25,'#12486#12522#12540#12492#39006
        '26,'#12524#12488#12523#12488#39006#28271#29006
        '27,'#20919#20941#35201#20919#12289#39321#36763#26009
        '28,'#12362#12388#12414#12415#35910#39006
        '29,'#12362#12388#12414#12415#12362#12363#12365#39006
        '30,'#12362#12388#12414#12415#12481#12483#12503#39006
        '31,'#12481#12519#12467#12524#12540#12488#12289#12476#12522#12540#39006
        '32,'#28023#29987#29289
        '33,'#24178#12375#32905
        ''
        '')
    end
    object CBName: TComboBox
      Left = 102
      Top = 31
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
      OnExit = CBNameExit
      OnKeyDown = CBNameKeyDown
    end
    object Memo: TMemo
      Left = 423
      Top = 216
      Width = 89
      Height = 20
      Alignment = taRightJustify
      ImeMode = imSAlpha
      Lines.Strings = (
        'Memo1')
      TabOrder = 2
      OnKeyPress = MemoKeyPress
    end
    object Button2: TButton
      Left = 528
      Top = 216
      Width = 75
      Height = 25
      Caption = #19968#25324#21453#26144#8595
      TabOrder = 3
      OnClick = Button2Click
    end
    object cmbCourse: TComboBox
      Left = 100
      Top = 96
      Width = 145
      Height = 20
      ItemHeight = 12
      TabOrder = 4
      Items.Strings = (
        '1'#35506#12288#26032#23487#12467#12540#12473'(11)'
        '1'#35506#12288#37504#24231#12467#12540#12473'(12)'
        '1'#35506#12288#21513#31077#23546#12467#12540#12473'(13)'
        '--------------------'
        '2'#35506#12288#28171#35895#12467#12540#12473'(22)'
        '2'#35506#12288#20845#26412#26408#12467#12540#12473'(23)'
        '2'#35506#12288#35199#40635#24067#12467#12540#12473'(24)'
        '--------------------'
        '3'#35506#12288#33970#30000#35199#12467#12540#12473'(31)'
        '3'#35506#12288#33970#30000#26481#12467#12540#12473'(32)'
        '3'#35506#12288#24029#23822#12467#12540#12473'(33)'
        '3'#35506#12288#33258#30001#65401#19992#12467#12540#12473'(34)'
        '--------------------'
        ' '#35506#12288#38609#12467#12540#12473'(41)')
    end
    object CBYomi: TComboBox
      Left = 102
      Top = 58
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imHira
      ItemHeight = 13
      ParentFont = False
      TabOrder = 5
      OnKeyDown = CBYomiKeyDown
    end
    object CBCode2: TComboBox
      Left = 354
      Top = 6
      Width = 61
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 6
      OnKeyDown = CBCode2KeyDown
    end
    object CBYyyy: TComboBox
      Left = 102
      Top = 140
      Width = 57
      Height = 20
      ItemHeight = 12
      TabOrder = 7
      Text = '2001'
      Items.Strings = (
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020')
    end
    object CBMm: TComboBox
      Left = 174
      Top = 140
      Width = 55
      Height = 20
      ItemHeight = 12
      TabOrder = 8
      Text = '01'
      Items.Strings = (
        '01'
        '02'
        '03'
        '04'
        '05'
        '06'
        '07'
        '08'
        '09'
        '10'
        '11'
        '12')
    end
    object CBYyyyTo: TComboBox
      Left = 270
      Top = 140
      Width = 57
      Height = 20
      ItemHeight = 12
      TabOrder = 9
      Text = '2001'
      Items.Strings = (
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020')
    end
    object CBMmTo: TComboBox
      Left = 342
      Top = 140
      Width = 55
      Height = 20
      ItemHeight = 12
      TabOrder = 10
      Text = '01'
      Items.Strings = (
        '01'
        '02'
        '03'
        '04'
        '05'
        '06'
        '07'
        '08'
        '09'
        '10'
        '11'
        '12')
    end
    object Button1: TButton
      Left = 448
      Top = 162
      Width = 75
      Height = 25
      Caption = #34920#31034
      TabOrder = 11
      OnClick = Button1Click
    end
    object EditChainCode: TEdit
      Left = 100
      Top = 187
      Width = 45
      Height = 20
      ImeMode = imClose
      TabOrder = 12
    end
    object StaticText1: TStaticText
      Left = 18
      Top = 100
      Width = 52
      Height = 16
      Caption = #12467#12540#12473' '#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 14
    end
    object StaticText2: TStaticText
      Left = 18
      Top = 143
      Width = 68
      Height = 16
      Caption = #38971#24230#26399#26085' '#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 15
    end
    object StaticText3: TStaticText
      Left = 234
      Top = 143
      Width = 30
      Height = 16
      Caption = #26376#65374
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 16
    end
    object StaticText4: TStaticText
      Left = 402
      Top = 143
      Width = 17
      Height = 16
      Caption = #26376
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 17
    end
    object StaticText5: TStaticText
      Left = 18
      Top = 191
      Width = 62
      Height = 16
      Caption = #12481#12455#12540#12531' '#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 18
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 693
    Width = 853
    Height = 41
    Align = alBottom
    Color = clTeal
    TabOrder = 4
    object BitBtn1: TBitBtn
      Left = 520
      Top = 8
      Width = 89
      Height = 25
      Caption = #38281#12376#12427'(&Z)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkIgnore
    end
    object BitBtn2: TBitBtn
      Left = 256
      Top = 8
      Width = 0
      Height = 25
      Caption = #12456#12463#12475#12523#12408#20986#21147'(&E)'
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Visible = False
      Kind = bkRetry
    end
    object BitBtn3: TBitBtn
      Left = 392
      Top = 8
      Width = 113
      Height = 25
      Caption = #19968#25324#21453#26144
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn3Click
      Kind = bkRetry
    end
  end
  object Query1: TQuery
    DatabaseName = 'dabMitsumori'
    Left = 616
    Top = 17
  end
  object dabMitsumori: TDatabase
    AliasName = 'taka'
    DatabaseName = 'dabMitsumori'
    LoginPrompt = False
    Params.Strings = (
      'user_name=sa'
      'password=netsurf'
      'USER NAME=sa')
    SessionName = 'Default'
    Left = 792
    Top = 24
  end
end
