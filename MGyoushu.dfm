inherited frmMGyoushu: TfrmMGyoushu
  Left = 705
  Top = 423
  Width = 506
  Height = 509
  Caption = 'frmMGyoushu'
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 498
    inherited Label1: TLabel
      Width = 94
      Caption = #26989#31278#12487#12540#12479
    end
    inherited Label2: TLabel
      Left = 184
    end
  end
  inherited Panel2: TPanel
    Top = 422
    Width = 498
    inherited BitBtn1: TBitBtn
      Left = 14
    end
    inherited BitBtn2: TBitBtn
      Visible = False
    end
  end
  inherited Panel3: TPanel
    Width = 498
    Height = 66
    Align = alTop
    object Label3: TLabel
      Left = 14
      Top = 12
      Width = 34
      Height = 13
      Caption = #12467#12540#12489
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 124
      Top = 12
      Width = 42
      Height = 13
      Caption = #26989#31278#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BitBtn3: TBitBtn
      Left = 320
      Top = 6
      Width = 75
      Height = 25
      Caption = #30331#12288#37682
      Default = True
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn3Click
    end
    object EditCode: TEdit
      Left = 58
      Top = 8
      Width = 53
      Height = 20
      ImeMode = imClose
      MaxLength = 3
      TabOrder = 1
    end
    object EditName: TEdit
      Left = 172
      Top = 8
      Width = 139
      Height = 20
      ImeMode = imOpen
      MaxLength = 100
      TabOrder = 2
    end
    object DBNavigator1: TDBNavigator
      Left = 4
      Top = 36
      Width = 240
      Height = 25
      DataSource = DS1
      TabOrder = 3
    end
  end
  inherited SB1: TStatusBar
    Top = 463
    Width = 498
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 107
    Width = 498
    Height = 315
    Align = alClient
    DataSource = DS1
    TabOrder = 4
    TitleFont.Charset = SHIFTJIS_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #65325#65331' '#65328#12468#12471#12483#12463
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Code'
        Title.Caption = #12467#12540#12489
        Title.Font.Charset = SHIFTJIS_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Title.Font.Style = [fsBold]
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Name'
        Title.Caption = #26989#31278#21517
        Title.Font.Charset = SHIFTJIS_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Title.Font.Style = [fsBold]
        Width = 244
        Visible = True
      end>
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    RequestLive = True
    SQL.Strings = (
      'SELECT * FROM tblMList')
    Left = 230
    Top = 15
    object Query1ID: TIntegerField
      FieldName = 'ID'
      Origin = 'TAKA.tblMList.ID'
    end
    object Query1Code: TStringField
      FieldName = 'Code'
      Origin = 'TAKA.tblMList.Code'
      FixedChar = True
      Size = 4
    end
    object Query1Name: TStringField
      FieldName = 'Name'
      Origin = 'TAKA.tblMList.Name'
      Size = 255
    end
    object Query1FieldCode: TStringField
      FieldName = 'FieldCode'
      Origin = 'TAKA.tblMList.FieldCode'
      FixedChar = True
      Size = 2
    end
  end
  object DS1: TDataSource
    DataSet = Query1
    Left = 266
    Top = 14
  end
end
