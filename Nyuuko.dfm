inherited frmNyuuko: TfrmNyuuko
  Left = 439
  Top = 138
  Width = 620
  Height = 473
  Caption = 'frmNyuuko'
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 604
    Height = 65
    inherited Label1: TLabel
      Width = 120
      Caption = #20837#24235#20837#21147#30011#38754
    end
    inherited Label2: TLabel
      Left = 34
      Top = 40
    end
    object Label7: TLabel
      Left = 300
      Top = 4
      Width = 56
      Height = 13
      Caption = #20253#31080#30058#21495
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cmbDenpyouBanngou: TComboBox
      Left = 304
      Top = 20
      Width = 113
      Height = 20
      ItemHeight = 12
      TabOrder = 0
    end
    object cmdDenpyouBanngouListMake: TButton
      Left = 430
      Top = 22
      Width = 121
      Height = 17
      Caption = #20253#31080#30058#21495#12522#12473#12488#20316#25104
      TabOrder = 1
      OnClick = cmdDenpyouBanngouListMakeClick
    end
    object cmdKennsaku: TButton
      Left = 558
      Top = 22
      Width = 49
      Height = 17
      Caption = #26908#32034
      TabOrder = 2
      OnClick = cmdKennsakuClick
    end
  end
  inherited Panel2: TPanel
    Top = 374
    Width = 604
    object lSum: TLabel [0]
      Left = 48
      Top = 16
      Width = 56
      Height = 13
      Caption = #21512#35336#37329#38989
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lbiInputNo: TLabel [1]
      Left = 593
      Top = 24
      Width = 4
      Height = 12
    end
    inherited BitBtn1: TBitBtn
      Left = 448
      Width = 97
    end
    inherited BitBtn2: TBitBtn
      Left = 336
      Width = 107
      Caption = #12503#12524#12499#12517#12540
      OnClick = BitBtn2Click
    end
  end
  inherited Panel3: TPanel
    Top = 65
    Width = 604
    Height = 104
    Align = alTop
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 48
      Height = 15
      Caption = #32013#21697#26085
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 36
      Width = 48
      Height = 15
      Caption = #20837#21147#32773
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 32
      Top = 60
      Width = 26
      Height = 13
      Caption = #12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 210
      Top = 8
      Width = 84
      Height = 13
      Caption = #20181#20837#20808#20808#26908#32034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 16
      Top = 77
      Width = 285
      Height = 13
      Caption = #21830#21697#26908#32034#12399'"'#12424#12415'"'#12398#19968#37096#12434#20837#21147#12375#12390'Go'#12508#12479#12531#12434#25276#19979
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 312
      Top = 36
      Width = 26
      Height = 15
      Caption = #12513#12514
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 208
      Top = 36
      Width = 32
      Height = 15
      Caption = #26376#20998
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbShiiresakiCode: TLabel
      Left = 546
      Top = 8
      Width = 5
      Height = 13
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DTP1: TDateTimePicker
      Left = 70
      Top = 6
      Width = 117
      Height = 23
      Date = 37111.466265312500000000
      Time = 37111.466265312500000000
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnChange = DTP1Change
    end
    object CBNyuuryokusha: TComboBox
      Left = 70
      Top = 31
      Width = 131
      Height = 20
      ImeMode = imClose
      ItemHeight = 12
      TabOrder = 1
      Items.Strings = (
        '001  '#39640#27211'(K)'
        '002  '#39640#27211'(T)'
        '003  '#26032#20117
        '004  '#37428#26408
        '005  '#22810#36032#35895
        '006  '#23567#29577)
    end
    object edbYomi: TEdit
      Left = 70
      Top = 56
      Width = 121
      Height = 20
      ImeMode = imOpen
      TabOrder = 4
      OnChange = edbYomiChange
    end
    object CBShiiresakiName: TComboBox
      Left = 304
      Top = 4
      Width = 233
      Height = 20
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ImeMode = imOpen
      ItemHeight = 12
      ParentFont = False
      TabOrder = 2
      OnExit = CBShiiresakiNameExit
      OnKeyDown = CBShiiresakiNameKeyDown
    end
    object cmdGo: TButton
      Left = 196
      Top = 58
      Width = 41
      Height = 17
      Caption = 'Go'
      Default = True
      TabOrder = 3
      OnClick = cmdGoClick
    end
    object MemoBikou: TMemo
      Left = 344
      Top = 31
      Width = 267
      Height = 19
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      Lines.Strings = (
        '')
      ParentFont = False
      TabOrder = 5
    end
    object RadioGroup1: TRadioGroup
      Left = 328
      Top = 55
      Width = 145
      Height = 41
      Caption = #12424#12415#20351#29992'/'#19981#20351#29992
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        #20351#29992
        #19981#20351#29992)
      TabOrder = 6
    end
    object EditGatsubun: TEdit
      Left = 246
      Top = 31
      Width = 51
      Height = 20
      ImeMode = imOpen
      TabOrder = 7
      Text = '2006/01'
    end
  end
  inherited SB1: TStatusBar
    Top = 415
    Width = 604
  end
  object SG1: TStringGrid
    Left = 0
    Top = 169
    Width = 604
    Height = 205
    Align = alClient
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 4
    OnDrawCell = SG1DrawCell
    OnKeyDown = SG1KeyDown
  end
  object QueryMShiiresaki: TQuery
    DatabaseName = 'taka'
    Left = 232
    Top = 9
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 264
    Top = 9
  end
  object QueryDenpyou: TQuery
    DatabaseName = 'taka'
    Left = 264
    Top = 41
  end
end
