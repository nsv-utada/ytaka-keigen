object dlgPasswordDlg: TdlgPasswordDlg
  Left = 389
  Top = 122
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #12497#12473#12527#12540#12489#12480#12452#12450#12525#12464
  ClientHeight = 93
  ClientWidth = 233
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 8
    Top = 9
    Width = 96
    Height = 12
    Caption = #12497#12473#12527#12540#12489#12398#20837#21147' :'
  end
  object Password: TEdit
    Left = 8
    Top = 27
    Width = 217
    Height = 20
    ImeMode = imDisable
    PasswordChar = '*'
    TabOrder = 0
  end
  object OKBtn: TButton
    Left = 70
    Top = 59
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 150
    Top = 59
    Width = 75
    Height = 25
    Cancel = True
    Caption = #12461#12515#12531#12475#12523
    Enabled = False
    ModalResult = 2
    TabOrder = 2
    Visible = False
  end
end
