inherited frmShiharaiNyuuryoku: TfrmShiharaiNyuuryoku
  Left = 562
  Top = 66
  Width = 501
  Height = 355
  Caption = 'frmShiharaiNyuuryoku'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 493
    inherited Label1: TLabel
      Width = 120
      Caption = #25903#25173#20837#21147#30011#38754
    end
    inherited Label2: TLabel
      Left = 234
    end
  end
  inherited Panel2: TPanel
    Top = 268
    Width = 493
    inherited BitBtn1: TBitBtn
      Left = 360
    end
    inherited BitBtn2: TBitBtn
      Width = 97
      Caption = #30331#37682
      OnClick = BitBtn2Click
    end
  end
  inherited Panel3: TPanel
    Width = 493
    Height = 227
    object Label3: TLabel
      Left = 30
      Top = 12
      Width = 81
      Height = 12
      Caption = #20181#20837#20808#12467#12540#12489#65297
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 30
      Top = 37
      Width = 24
      Height = 12
      Caption = #12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 30
      Top = 62
      Width = 52
      Height = 12
      Caption = #20181#20837#20808#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 32
      Top = 104
      Width = 36
      Height = 12
      Caption = #25903#25173#26085
    end
    object Label7: TLabel
      Left = 32
      Top = 123
      Width = 24
      Height = 12
      Caption = #37329#38989
    end
    object Label20: TLabel
      Left = 252
      Top = 104
      Width = 42
      Height = 13
      Caption = #20309#26376#20998
      Color = clScrollBar
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label14: TLabel
      Left = 30
      Top = 184
      Width = 33
      Height = 13
      Caption = #12513#12288#12514
      Color = clScrollBar
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object labelZan4: TLabel
      Left = 256
      Top = 126
      Width = 79
      Height = 12
      Caption = #27531#39640#65288#31246#36796#12415#65289
      Color = clScrollBar
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object Label8: TLabel
      Left = 32
      Top = 147
      Width = 59
      Height = 12
      Caption = #25903#25173#12356#26041#27861
    end
    object Label9: TLabel
      Left = 384
      Top = 64
      Width = 43
      Height = 12
      Caption = 'lShimebi'
    end
    object CBName: TComboBox
      Left = 126
      Top = 56
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnExit = CBNameExit
      OnKeyDown = CBNameKeyDown
    end
    object EditYomi: TEdit
      Left = 126
      Top = 33
      Width = 243
      Height = 20
      ImeMode = imOpen
      TabOrder = 1
      OnKeyDown = EditYomiKeyDown
    end
    object CBShiiresakiCode: TComboBox
      Left = 126
      Top = 8
      Width = 97
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 2
      OnKeyDown = CBShiiresakiCodeKeyDown
    end
    object CBShiharai: TCheckBox
      Left = 32
      Top = 80
      Width = 97
      Height = 17
      Caption = #25903#25173#12356
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = CBShiharaiClick
      OnExit = CBShiharaiExit
    end
    object CBTax: TCheckBox
      Left = 96
      Top = 80
      Width = 97
      Height = 17
      Caption = #28040#36027#31246#35519#25972
      TabOrder = 4
      OnClick = CBTaxClick
      OnExit = CBTaxExit
    end
    object DTP1: TDateTimePicker
      Left = 126
      Top = 98
      Width = 107
      Height = 20
      Date = 38754.873553958340000000
      Time = 38754.873553958340000000
      TabOrder = 5
      OnExit = DTP1Exit
    end
    object EditKingaku: TEdit
      Left = 126
      Top = 120
      Width = 107
      Height = 20
      ImeMode = imDisable
      TabOrder = 6
    end
    object EditGatsubun: TMaskEdit
      Left = 302
      Top = 98
      Width = 61
      Height = 20
      EditMask = '9999/99;1;_'
      ImeMode = imClose
      MaxLength = 7
      TabOrder = 7
      Text = '    /  '
      OnExit = EditGatsubunExit
    end
    object Memo1: TMemo
      Left = 87
      Top = 172
      Width = 385
      Height = 33
      ImeMode = imOpen
      TabOrder = 8
    end
    object EditZandaka: TMemo
      Left = 334
      Top = 122
      Width = 81
      Height = 22
      Alignment = taRightJustify
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 9
      Visible = False
    end
    object CBNyukinStatus: TComboBox
      Left = 126
      Top = 144
      Width = 123
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 10
      Text = '1,'#23567#20999#25163
      OnKeyDown = CBShiiresakiCodeKeyDown
      Items.Strings = (
        '1.'#23567#20999#25163#13
        '2.'#25163#24418
        '3.'#25391#12426#36796#12415
        '4.'#29694#37329
        '5.'#28040#36027#31246#35519#25972#12394#12393)
    end
  end
  inherited SB1: TStatusBar
    Top = 309
    Width = 493
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 172
    Top = 11
  end
  object QueryInShiire: TQuery
    DatabaseName = 'taka'
    Left = 204
    Top = 11
  end
  object QueryINShiireDetail: TQuery
    DatabaseName = 'taka'
    Left = 236
    Top = 11
  end
  object QueryShimebi: TQuery
    DatabaseName = 'taka'
    Left = 268
    Top = 11
  end
end
