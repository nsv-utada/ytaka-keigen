inherited frmShukko: TfrmShukko
  Left = 784
  Top = 350
  VertScrollBar.Range = 0
  BorderStyle = bsSingle
  Caption = #20986#24235#21360#21047
  ClientHeight = 240
  ClientWidth = 456
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 456
    inherited Label1: TLabel
      Caption = #20986#24235#34920#21360#21047
    end
    inherited Label2: TLabel
      Left = 196
    end
  end
  inherited Panel2: TPanel
    Top = 187
    Width = 456
    Height = 35
    inherited BitBtn1: TBitBtn
      Left = 317
      Height = 23
      Cancel = True
      Kind = bkCustom
    end
    inherited BitBtn2: TBitBtn
      Left = 220
      Width = 90
      Height = 23
      Caption = #20986#24235#34920#21360#21047
      OnClick = BitBtn2Click
      Glyph.Data = {00000000}
      Kind = bkCustom
    end
  end
  inherited Panel3: TPanel
    Width = 456
    Height = 146
    object Label3: TLabel
      Left = 22
      Top = 42
      Width = 50
      Height = 13
      Caption = #12467#12540#12473' '#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 254
      Top = 44
      Width = 44
      Height = 13
      Caption = #12501#12525#12450#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 22
      Top = 70
      Width = 50
      Height = 13
      Caption = #12467#12540#12473' '#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 226
      Top = 56
      Width = 16
      Height = 15
      Caption = #65286
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 22
      Top = 18
      Width = 50
      Height = 13
      Caption = #32013#21697#26085#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cmbCourse1: TComboBox
      Left = 76
      Top = 40
      Width = 145
      Height = 20
      ItemHeight = 12
      TabOrder = 0
      Items.Strings = (
        '1'#35506#12288#26032#23487#12467#12540#12473'(11)'
        '1'#35506#12288#37504#24231#12467#12540#12473'(12)'
        '1'#35506#12288#21513#31077#23546#12467#12540#12473'(13)'
        '--------------------'
        '2'#35506#12288#28171#35895#12467#12540#12473'(22)'
        '2'#35506#12288#20845#26412#26408#12467#12540#12473'(23)'
        '2'#35506#12288#35199#40635#24067#12467#12540#12473'(24)'
        '--------------------'
        '3'#35506#12288#33970#30000#35199#12467#12540#12473'(31)'
        '3'#35506#12288#33970#30000#26481#12467#12540#12473'(32)'
        '3'#35506#12288#24029#23822#12467#12540#12473'(33)'
        '3'#35506#12288#33258#30001#65401#19992#12467#12540#12473'(34)'
        '--------------------'
        ' '#35506#12288#38609#12467#12540#12473'(41)')
    end
    object chk1F: TCheckBox
      Left = 320
      Top = 45
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = '1F'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chk2F: TCheckBox
      Left = 320
      Top = 63
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = '2F'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object chkRT: TCheckBox
      Left = 384
      Top = 81
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = 'RT'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object chkAll: TCheckBox
      Left = 24
      Top = 114
      Width = 131
      Height = 17
      Alignment = taLeftJustify
      Caption = #20840#12467#12540#12473#21512#35336
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnMouseDown = chkAllMouseDown
    end
    object cmbCourse2: TComboBox
      Left = 76
      Top = 68
      Width = 145
      Height = 20
      ItemHeight = 12
      TabOrder = 5
      Items.Strings = (
        '1'#35506#12288#26032#23487#12467#12540#12473'(11)'
        '1'#35506#12288#37504#24231#12467#12540#12473'(12)'
        '1'#35506#12288#21513#31077#23546#12467#12540#12473'(13)'
        '--------------------'
        '2'#35506#12288#28171#35895#12467#12540#12473'(22)'
        '2'#35506#12288#20845#26412#26408#12467#12540#12473'(23)'
        '2'#35506#12288#35199#40635#24067#12467#12540#12473'(24)'
        '--------------------'
        '3'#35506#12288#33970#30000#35199#12467#12540#12473'(31)'
        '3'#35506#12288#33970#30000#26481#12467#12540#12473'(32)'
        '3'#35506#12288#24029#23822#12467#12540#12473'(33)'
        '3'#35506#12288#33258#30001#65401#19992#12467#12540#12473'(34)'
        '--------------------'
        ' '#35506#12288#38609#12467#12540#12473'(41)')
    end
    object dtpNouhinDate: TDateTimePicker
      Left = 76
      Top = 12
      Width = 110
      Height = 20
      Date = 37297.655360127300000000
      Time = 37297.655360127300000000
      TabOrder = 6
    end
    object chkHK: TCheckBox
      Left = 384
      Top = 45
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = 'HK'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object chkQS: TCheckBox
      Left = 384
      Top = 63
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = 'QS'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object chkRZ: TCheckBox
      Left = 384
      Top = 99
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = 'RZ'
      Checked = True
      State = cbChecked
      TabOrder = 9
    end
    object chk2FA: TCheckBox
      Left = 320
      Top = 81
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = '2F-A'
      Checked = True
      State = cbChecked
      TabOrder = 10
    end
    object chk2FB: TCheckBox
      Left = 320
      Top = 99
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = '2F-B'
      Checked = True
      State = cbChecked
      TabOrder = 11
    end
    object chkST: TCheckBox
      Left = 384
      Top = 117
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = 'ST'
      Checked = True
      State = cbChecked
      TabOrder = 12
    end
  end
  inherited SB1: TStatusBar
    Top = 222
    Width = 456
    Height = 18
  end
  object qryShukko: TQuery
    DatabaseName = 'taka'
    Left = 264
    Top = 97
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 264
    Top = 129
  end
end
