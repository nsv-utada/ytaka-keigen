unit Shukko;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables,ShellAPI;

type
  TfrmShukko = class(TfrmMaster)
    Label3: TLabel;
    cmbCourse1: TComboBox;
    Label4: TLabel;
    chk1F: TCheckBox;
    chk2F: TCheckBox;
    chkRT: TCheckBox;
    chkAll: TCheckBox;
    Label5: TLabel;
    cmbCourse2: TComboBox;
    Label8: TLabel;
    qryShukko: TQuery;
    dtpNouhinDate: TDateTimePicker;
    Label6: TLabel;
    chkHK: TCheckBox;
    chkQS: TCheckBox;
    chkRZ: TCheckBox;
    Query1: TQuery;
    chk2FA: TCheckBox;
    chk2FB: TCheckBox;
    chkST: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure chkAllMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn2Click(Sender: TObject);
    function GetCourseID(CourseName: String) : String;
    function GetShiiresaki(sCode1,sCode2: String) : String;

  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmShukko: TfrmShukko;

implementation

uses Inter, HKLib;

{$R *.DFM}

procedure TfrmShukko.FormCreate(Sender: TObject);
var
 sSql : String;
begin

  // 納品日を当日に設定
  dtpNouhinDate.Date := Date();

  // コンボボックスクリア
  cmbCourse1.Items.Clear;
  cmbCourse2.Items.Clear;

try

  // コンボボックスリスト作成
	sSql := 'SELECT CourseName FROM ' + CtblMCourse;
  sSql := sSql + ' order by CourseID ';

  cmbCourse1.Clear;
  cmbCourse2.Clear;

	with qryShukko do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	cmbCourse1.Items.Add(FieldByName('CourseName').AsString);
    	cmbCourse2.Items.Add(FieldByName('CourseName').AsString);
    	Next;
    end;
    Close;
  end;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


procedure TfrmShukko.chkAllMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin

  if chkAll.Checked = False then
    begin
      // コースをクリアして入力不可にする
      // (本当はチェックされている時，つまり Checked = Trueの時これを行う筈だが，
      // 何故か実行時の動きが逆になる．よってfalseの時以下の処理を行う様にしている．
      // Delphiのバグ？)
      cmbCourse1.Text    := '';
      cmbCourse2.Text    := '';
      cmbCourse1.Enabled := False;
      cmbCourse2.Enabled := False;
      cmbCourse1.Color   := clMenu;
      cmbCourse2.Color   := clMenu;
    end
  else if chkAll.Checked = True then
    begin
      // コースをクリアして入力可能にする
      cmbCourse1.Text    := '';
      cmbCourse2.Text    := '';
      cmbCourse1.Enabled := True;
      cmbCourse2.Enabled := True;
      cmbCourse1.Color   := clWindow;
      cmbCourse2.Color   := clWindow;
    end
  else
    begin
      ShowMessage('エラー発生');
    end;

end;


procedure TfrmShukko.BitBtn2Click(Sender: TObject);
//
// 出庫表を印刷する．
//
var
  sLine            : String;
 	F                : TextFile;
  sNouhinDate      : String;
  sSql             : String;
  sWhere           : String;
  sList            : String;
  GResult          : array of array of String;
  i,iRecordCount   : Integer;

begin

// 確認メッセージ出力

  if MessageDlg('出力しますか?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
    begin
      Exit;
    end;

//出力するファイルを作成する、すでにあれば削除する

 	AssignFile(F, CFileName_Shukkohyou);
  Rewrite(F);
	CloseFile(F);

//タイトル部分の出力

  DateTimeToString(sNouhinDate, 'yyyy/mm/dd', dtpNouhinDate.Date);

  sLine := '納品日  ' + sNouhinDate;
  HMakeFile(CFileName_Shukkohyou, sLine);

  sLine := '';
  HMakeFile(CFileName_Shukkohyou, sLine);

  if chkAll.Checked = True then
    begin
      sLine := 'コース :   全コース分';
    end
  else
    begin
      sLine := 'コース : ' + cmbCourse1.Text + '   ' + cmbCourse2.Text;
    end;
  HMakeFile(CFileName_Shukkohyou, sLine);

//
// SQL作成・実行 〜 抽出データを GResultに格納する．
//

// Where部作成

  // �@ 納品日
  sWhere := 'WHERE ( D.NouhinDate = ' + '''' + sNouhinDate + ''')';

  // �A コース
  if chkAll.Checked = False then
    begin
     // コース１，コース２共に指定
     if (cmbCourse1.Text <> '') and (cmbCourse2.Text <> '') then
       begin
         sWhere := sWhere + ' AND ( T.TokuisakiCode2 = ' + '''' + GetCourseID(cmbCourse1.Text) + '''';
         sWhere := sWhere + ' OR T.TokuisakiCode2 = ' + '''' + GetCourseID(cmbCourse2.Text) + ''')';
       end
     // コース１指定，コース２が空
     else if (cmbCourse1.Text <> '') and (cmbCourse2.Text = '') then
       begin
         sWhere := sWhere + ' AND ( T.TokuisakiCode2 = ' + '''' + GetCourseID(cmbCourse1.Text) + ''')';
       end
     // コース１空，コース２指定
     else if (cmbCourse1.Text = '') and (cmbCourse2.Text <> '') then
       begin
         sWhere := sWhere + ' AND ( T.TokuisakiCode2 = ' + '''' + GetCourseID(cmbCourse2.Text) + ''')';
       end
     // コース１空，コース２空　（→ コース＝''つまり0件が抽出される）
     else if (cmbCourse1.Text = '') and (cmbCourse2.Text = '') then
       begin
         sWhere := sWhere + ' AND ( T.TokuisakiCode2 = '''' )';
       end;
    end;


  // �B 階数
  //   チェックが1個でもついている場合は，以下の処理に入る．
  //　 チェックが1個もついていない場合は，F1=''つまり0件が抽出される．
  if (chk1F.Checked = True) or (chk2F.Checked = True)
     or (chkRT.Checked = True) or (chkRZ.Checked = True)
     or (chkHK.Checked = True) or (chkQS.Checked = True)
     or (chk2FA.Checked = True) or (chk2FB.Checked = True) or (chkST.Checked = True) // add 20121009 棚宛変更対応

  then begin

     sList := '( ';

    if chk1F.Checked = True then
      sList := sList +  '''1F'', ';
    if chk2F.Checked = True then
      sList := sList +  '''2F'', ';
    if chkRT.Checked = True then
      sList := sList +  '''RT'', ';
    if chkRZ.Checked = True then
      sList := sList +  '''RZ'', ';
    if chkHK.Checked = True then
      sList := sList +  '''HK'', ';
    if chkQS.Checked = True then
      sList := sList +  '''QS'', ';
    // add 20121009 棚宛変更対応
    if chk2FA.Checked = True then
      sList := sList +  '''2F-A'', ';
    if chk2FB.Checked = True then
      sList := sList +  '''2F-B'', ';
    if chkST.Checked = True then
      sList := sList +  '''ST'', ';

    sList := sList + ' ''XX'' )';
     // 上のif群でリストを作成しても最後のカンマがどうしても余る.
     // 例えば，1Fと2Fがチェックされている場合，('1F','2F',)となり，
     // 最後のカンマがどうしても余ってしまう．どうしようも無いので，
     // 絶対有りえないXXを最後に加えて，('1F','2F','XX' )と逃げている．
    sWhere := sWhere + ' AND F1 IN ' + sList;

  end else
    begin
      sWhere := sWhere + ' AND ( F1 = '''' )';
    end;


// SQL作成

  sSql := 'SELECT Tanni,Irisuu,I.F1 F1, I.F2 F2, I.Code1 Code1, I.Code2 Code2, ';
  sSql := sSql + 'I.Name Name, SUM(D.Suuryou) AS Suuryou FROM ' + CtblMItem;
  sSql := sSql + ' I INNER JOIN ' + CtblTDenpyouDetail + ' D ON';
  sSql := sSql + ' I.Code1 = D.Code1 AND I.Code2 = D.Code2 ';
  sSql := sSql + ' INNER JOIN ' + CtblMTokuisaki + ' T ON';
  sSql := sSql + ' D.TokuisakiCode1 = T.TokuisakiCode1 ';
  sSql := sSql + sWhere;
  sSql := sSql + ' GROUP BY I.F1, I.F2, I.Code1, I.Code2, I.Name, I.Irisuu,I.Tanni';
  sSql := sSql + ' ORDER BY I.F1, I.F2, I.Code1, I.Code2';

  // データをとりあえず GResultに格納する．

try
  SB1.SimpleText := sSql;
  with qryShukko do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then
      begin
        ShowMessage('本日の出庫対象は存在しません．');
        Close;
        Exit;
      end;

    // 動的配列の次元数を決定
    iRecordCount := RecordCount;         // 次のFor文に向けてレコードカウントを保存

    //2002.08.10
    //SetLength(GResult, RecordCount, 6);
    //SetLength(GResult, RecordCount, 8);
    //2005.06.16
    SetLength(GResult, RecordCount, 9);

    // データをGResultに格納
    i := 0;
    While not EOF do begin
 	    GResult[i,   0] := FieldbyName('F1').AsString;
 	    GResult[i,   1] := FieldbyName('F2').AsString;
 	    GResult[i,   2] := FieldbyName('Code1').AsString;
 	    GResult[i,   3] := FieldbyName('Code2').AsString;
 	    GResult[i,   4] := FieldbyName('Name').AsString;
 	    GResult[i,   5] := FieldbyName('Suuryou').AsString;
      //added by H.K. 2002/08/10
 	    GResult[i,   6] := FieldbyName('Irisuu').AsString;
 	    GResult[i,   7] := FieldbyName('Tanni').AsString;

      //added by H.K. 2005/06/16
      //仕入先コード、仕入先名、メーカー名
 	    GResult[i,   8] := GetShiiresaki(FieldbyName('Code1').AsString,
                                       FieldbyName('Code2').AsString
                                      );

      i := i + 1;
      SB1.SimpleText := FieldbyName('Name').AsString;
      next
    end;

    Close;

  end;


//
// データをテキストファイルへ書き出す．
//

   For i := 0 to iRecordCount - 1 do begin

    // 階数の出力
    if ( i=0 ) or ( GResult[i-1,0] <> GResult[i,0] ) then
      begin
        sLine := '階数 ： ' + GResult[i,0];
        HMakeFile(CFileName_Shukkohyou, sLine);
      end;

    // 棚の出力
    if ( i=0 ) or ( GResult[i-1,1] <> GResult[i,1] ) then
      begin
        sLine := ',棚数 ： ' + GResult[i,1];
        HMakeFile(CFileName_Shukkohyou, sLine);
      end;

    // Code1, Code2, Name, SUM(Suuryou)の出力
      //sLine := ',,' + GResult[i,2] + ',' + GResult[i,3] + ',';
      sLine := ',' + ',';
      //sLine := sLine + GResult[i,4] + ','+ GResult[i,5] + ',';
      sLine := sLine + GResult[i,4] + ','+ GResult[i,6] + ',';

      //2002.08.10
      sLine := sLine + GResult[i,5] + ',' + GResult[i,7];

      //2005.06.16
      sLine := sLine + ',' + GResult[i,8];

      HMakeFile(CFileName_Shukkohyou, sLine);


   end;

  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '出庫表bk.xls', '', SW_SHOW);

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;

//商品マスターから仕入先・メーカーを返す
function TfrmShukko.GetShiiresaki(sCode1,sCode2 : String) : String;
var
  sSql,sRet      : String;
begin
  sSql := 'SELECT ShiiresakiCode, Maker FROM ' + CtblMItem;
  sSql := sSql + ' WHERE Code1 = ' + '''' + sCode1 + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' Code2 = ' + '''' + sCode2 + '''';
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sRet :=FieldbyName('Maker').AsString;
    sRet :=sRet + ',' + FieldbyName('ShiiresakiCode').AsString;
    Close;
  end;

  Result := sRet;

end;


function TfrmShukko.GetCourseID(CourseName: String) : String;
var
	sSql       : String;
  sCourseID  : String;

begin

  // コースID
	sSql := 'SELECT CourseID FROM ' + CtblMCourse;
  sSql := sSql + ' WHERE CourseName = ' + '''' + CourseName + '''';
	with qryShukko do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sCourseID :=FieldbyName('CourseID').AsString;
    Close;
  end;

  Result := sCourseID;

end;


end.
