unit TDenpyou;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, DBTables, ShellAPI;

type
  TfrmTDenpyou = class(TfrmMaster)
    Label3: TLabel;
    Label4: TLabel;
    EditTokuisakiName: TEdit;
    Label6: TLabel;
    CBTokuisakiCode1: TEdit;
    CBTokuisakiCode2: TEdit;
    DTP1: TDateTimePicker;
    Label5: TLabel;
    Label7: TLabel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    CBUriageKingaku: TComboBox;
    Memo2: TMemo;
    Label8: TLabel;
    EditMemo: TComboBox;
    BitBtn5: TBitBtn;
    LabelID: TLabel;
    Label10: TLabel;
    Shape1: TShape;
    lbDenpyouCode: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure CBTokuisakiCode1Exit(Sender: TObject);
    procedure DTP1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn4Click(Sender: TObject);
    procedure CBUriageKingakuExit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure EditMemoExit(Sender: TObject);
    procedure EditMemoClick(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeCBUriageKingaku(sTokuisakiCode, sDDate:String);
		function Check2Juu(sTokuisakiCode1, sDDate, sUriageKingaku:String):Boolean;
		Function DeleteDenpyou : Boolean;
		Function DeleteDenpyou2 : Boolean;
		Function DeleteDenpyouDetail : Boolean;
		Function InsertDenpyou : Boolean;
    Function GetID : Integer;
    Function GetID2 : String;  //2002.05.28 H.Kubota
		Function Delete_tblTGennkinnKaishuu : Boolean;
  public
    { Public 宣言 }
  end;

var
  frmTDenpyou: TfrmTDenpyou;

implementation
uses
	Inter, MTokuisaki, DMMaster, HKLib,PasswordDlg;

{$R *.DFM}

procedure TfrmTDenpyou.FormCreate(Sender: TObject);
begin
  inherited;
  width := 600;
  height := 350;
  DTP1.Date := Date();

 dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
 if dlgPasswordDlg.ShowModal = mrOK then begin

 end else begin
 		Exit;
 end;
end;

//登録ボタンがクリックされた
//1999/09/10
// TDenpyouのGatsubun に何月分の売上かを入力する
procedure TfrmTDenpyou.BitBtn3Click(Sender: TObject);
var
	sSql,sTokuisakiCode1,sDDate,sKingaku : String;
  bFlag : Boolean;
begin
  //２重登録のチェック
  sTokuisakiCode1 := CBTokuisakiCode1.Text;
  sDDate := DateToStr(DTP1.Date);
  sKingaku := CBUriageKingaku.Text;
  bFlag := Check2Juu(sTokuisakiCode1, sDDate, sKingaku);
  if bFlag = True then begin
	  if MessageDlg('この伝票はすでに登録されています。' + #10#13 +
                  '上書きする場合はここで中止して削除してから再登録してください' +#10#13+
                  '中止しますか？',
  	  mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    	Exit;
	  end;
  end;

  if MessageDlg('登録しますか？',mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    	Exit;
  end;

	if InsertDenpyou = True then begin
	 	ShowMessage('登録に成功しました');
    //後処理
    EditMemo.Text := '';
	  CBTokuisakiCode1.Setfocus;
  end else begin
	  ShowMessage('登録に失敗しました');
  end;
end;

Function TfrmTDenpyou.InsertDenpyou : Boolean;
var
	sSeikyuuShimebi, sGatsubun, sSql, sDay, sMonth, sYear : String;
  wYyyy, wMm, wDd : Word;
begin

	//1999/09/10
	// TDenpyouのGatsubun に何月分の売上かを入力する
  //伝票日付が締め日を越えていたら月を１進める
  sSeikyuuShimebi := MTokuisaki.GetShimebi(CBTokuisakiCode1.Text);
  sGatsubun := DateToStr(DTP1.Date);

  DecodeDate(StrToDate(sGatsubun), wYyyy, wMm, wDd);

 	sYear  := IntToStr(wYyyy);
 	sMonth := IntToStr(wMm);
 	sDay   := IntToStr(wDd);

  if sSeikyuuShimebi = '0' then begin //月末ならそのまま
    sGatsubun := sYear + '/' + sMonth + '/01';
  end else begin
    if StrToInt(sDay) > StrToInt(sSeikyuuShimebi) then begin
    	if StrToInt(sMonth) = 12 then begin
        sMonth := '01';
        sYear := INtToStr(StrToInt(sYear) + 1);
      end else begin
      	sMonth := IntToStr(StrToInt(sMonth) + 1);
      end;
    end;
    sGatsubun := sYear + '/' + sMonth + '/01';
  end;



	try
		sSql := 'INSERT INTO ' + CtblTDenpyou;
		sSql := sSql + '(';
  	sSql := sSql + 'TokuisakiCode1, ';
		sSql := sSql + 'TokuisakiCode2, ';
		sSql := sSql + 'DNumber,        ';
	  sSql := sSql + 'InputDate,      ';
	  sSql := sSql + 'DDate,          ';
  	sSql := sSql + 'UriageKingaku,  ';
	  sSql := sSql + 'Memo,           ';
	  sSql := sSql + 'Gatsubun        '; //1999/09/10
		sSql := sSql + ') VALUES (';
		sSql := sSql + CBTokuisakiCode1.Text    + ',';
		sSql := sSql + CBTokuisakiCode2.Text    + ',';
		sSql := sSql + '0,'; //伝票ナンバーはとりあえず使わない
		sSql := sSql + '''' + DateToStr(Date) + ''',';
		sSql := sSql + '''' + DateToStr(DTP1.Date)            + ''',';
		sSql := sSql + CBUriageKingaku.Text       + ',';
		sSql := sSql + '''' + EditMemo.Text     + ''',';
		sSql := sSql + '''' + sGatsubun    + '''';   //199909/10
  	sSql := sSql + ')';
	 	with frmDMMaster.QueryInsert do begin
		 	Close;
	    Sql.Clear;
  		Sql.Add(sSql);
  		ExecSql;
	    Close;
  	end;//of with
    Result := True;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
	    Result := False;
    end;
  end;
end;

//伝票の２重チェック
function TfrmTDenpyou.Check2Juu(sTokuisakiCode1, sDDate, sUriageKingaku:String):Boolean;
var
	sSql : String;
begin
	sSql := 'SELECT TokuisakiCode1 ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' DDate =  ''' + sDDate + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' UriageKingaku =  ' + sUriageKingaku;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo = ''' + Editmemo.Text + '''';
  with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if EOF then begin
    	Result := False;
    end else begin
    	Result := True;
    end;
    Close;
  end;
end;

//伝票番号を取得する
function TfrmTDenpyou.GetID : Integer;
var
	sSql : String;
begin
	sSql := 'SELECT ID ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' DDate =  ''' + DateToStr(DTP1.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' TokuisakiCode1 =  ' + CBTokuisakiCode1.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + ' UriageKingaku =  ' + CBUriageKingaku.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo = ''' + Editmemo.Text + '''';
  with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if EOF then begin
    	Result := FieldByName('ID').AsInteger;
    end else begin
    	Result := FieldByName('ID').AsInteger;
    end;
    Close;
  end;
end;

//伝票番号を取得する
function TfrmTDenpyou.GetID2 : string;
var
	sSql : String;
begin
	sSql := 'SELECT DenpyouCode ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' DDate =  ''' + DateToStr(DTP1.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' TokuisakiCode1 =  ' + CBTokuisakiCode1.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + ' UriageKingaku =  ' + CBUriageKingaku.Text;
//Add Utada 2009/10
  sSql := sSql + ' AND ';
  sSql := sSql + ' bikou LIKE ''' + Editmemo.Text + '''';
  //sSql := sSql + ' DenpyouCode LIKE ''' + Editmemo.Text + '''';

  with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if EOF then begin
    	Result := '';
    end else begin
    	Result := FieldByName('DenpyouCode').asString;
    end;
    Close;
  end;
end;


//得意先コード２と得意先名を表示する
procedure TfrmTDenpyou.CBTokuisakiCode1Exit(Sender: TObject);
var
	sTokuisakiCode1 : String;
begin
	if CBTokuisakiCode1.text = '' then Exit;

	sTokuisakiCode1 := CBTokuisakiCode1.Text;
	EditTokuisakiName.Text := MTokuisaki.GetTokuisakiName(sTokuisakiCode1);
	CBTokuisakiCode2.Text  := MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1);

  //月２回締めの得意先の警告
  //1999/06/20
  if Inter.CheckTokuisakiCode1(sTokuisakiCode1) = True then begin
     ShowMessage('「' + EditTokuisakiName.Text + '」は月２回締めです。');
  	 Exit;
  end;

end;

//伝票の検索
procedure TfrmTDenpyou.DTP1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
	sTokuisakiCode, sDDate : String;
begin
	sTokuisakiCode := CBTokuisakiCode1.Text;
  sDDate := DateToStr(DTP1.Date);
	if (Key=VK_F1) then begin
  	LabelID.Caption := 'ID';
	  lbDenpyouCode.Caption := '';
	 	MakeCBUriageKingaku(sTokuisakiCode, sDDate);
  end;
end;

//売上げ金額のコンボボックスを作成する
procedure TfrmTDenpyou.MakeCBUriageKingaku(sTokuisakiCode, sDDate:String);
var
	sSql : String;
begin
  CBUriageKingaku.Items.Clear;

	sSql := 'SELECT UriageKingaku FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate = ''' + sDDate + '''';

	with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBUriageKingaku.Items.Add(FieldByName('UriageKingaku').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBUriageKingaku.DroppedDown := True;

end;

//削除ボタンがクリックされた
procedure TfrmTDenpyou.BitBtn4Click(Sender: TObject);
var
	sSql,sPwd: String;
begin
  //2002.06.29 削除時のパスワード
{
   sPwd := InputBox('パスワード入力',
    '伝票を削除するにはパスワードが必要です。',
    '');
   if sPwd<>'1357' then begin
	    ShowMessage('パスワードが違います');
      exit;
   end;
}
//2003.02.09
	dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
	if dlgPasswordDlg.ShowModal = mrOK then begin

	end else begin
		Exit;
	end;

	if GPassWord = CPassword1 then begin
    Beep;
  //コメントアウト 2009.04.08 utada
	//end else if GPassWord = CPassword2 then begin
  //  Beep;
	//end else if GPassWord = CPassword3 then begin
	//  ShowMessage('PassWordが違います');
  //  exit;
    //コメントアウト end
	end else begin
	  ShowMessage('PassWordが違います');
    exit;
	end;
	GPassWord := '';


  //2002.05.28
  //新伝票か旧伝票かの判断
  if lbDenpyouCode.Caption = '' then begin
	  //確認メッセージ
    if MessageDlg('旧伝票削除しますか?',
      mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
      Exit;
    end;
    if DeleteDenpyou = True then begin
  	  ShowMessage('旧伝票の削除に成功しました');
    end else begin
	    ShowMessage('削除に失敗しました');
    end;
  end else begin //新伝票
    if MessageDlg('新伝票削除しますか?',
      mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
      Exit;
    end;
    //tblTDenpyouの削除
    DeleteDenpyou2;
    //tblTDenpyouDetailの削除
    DeleteDenpyouDetail;

    //現金伝票の場合
    //2002.08.07
    if StrToInt(Trim(CBTokuisakiCode1.Text)) > 50000 then begin
	    ShowMessage('現金伝票なので未集金リストからも削除します');
      if Delete_tblTGennkinnKaishuu = False then begin
		    ShowMessage('未集金リストの削除に失敗しました。システム管理者にご連絡ください');
        exit;
      end;
    end;//of if


    //在庫テーブルに商品{ TODO 1 -ohajimekubota -c伝票関係 : 在庫商品を戻す }を戻す

 	  ShowMessage('新伝票の削除に成功しました');
  end;

  //後処理
  EditMemo.Text := '';
  CBUriageKingaku.Text := '';
  CBTokuisakiCode1.Setfocus;
  lbDenpyouCode.Caption := '';


end;


//現金伝票用
//2002.08.07
Function TfrmTDenpyou.Delete_tblTGennkinnKaishuu : Boolean;
var
	sSql : String;
  iID : Integer;
begin
	sSql := 'DELETE FROM ' + CtblTGennkinnKaishuu;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' DenpyouCode = ''' + lbDenpyouCode.Caption + '''';

  try
	  with frmDMMaster.QueryDelete do begin
  	  Close;
			Sql.Clear;
	    Sql.Add(sSql);
			ExecSql;
	  	Close;
	  end;//of with
    Result := True;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
	    Result := False;
    end;
  end;

end;


//現在の伝票を削除する
//1999/06/20
Function TfrmTDenpyou.DeleteDenpyou : Boolean;
var
	sSql : String;
  iID : Integer;
begin
	if (LabelID.Caption = 'ID') or (LabelID.Caption = '0') then begin
  	ShowMessage('伝票番号が特定できないため削除できません');
    Exit;
  end;

	sSql := 'DELETE FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' ID =  ' + LabelID.Caption;

  try
	  with frmDMMaster.QueryDelete do begin
  	  Close;
			Sql.Clear;
	    Sql.Add(sSql);
			ExecSql;
	  	Close;
	  end;//of with
    Result := True;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
	    Result := False;
    end;
  end;
end;

//新伝票を削除する
//2002.05.28
Function TfrmTDenpyou.DeleteDenpyou2 : Boolean;
var
	sSql : String;
  iID : Integer;
begin
	if (lbDenpyouCode.Caption = '') then begin
  	ShowMessage('伝票番号が特定できないため削除できません');
    Exit;
  end;

	sSql := 'DELETE FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' DenpyouCode =  ''' + lbDenpyouCode.Caption + '''';

  try
	  with frmDMMaster.QueryDelete do begin
  	  Close;
			Sql.Clear;
	    Sql.Add(sSql);
			ExecSql;
	  	Close;
	  end;//of with
    Result := True;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
	    Result := False;
    end;
  end;
end;

//新伝票詳細を削除する
//2002.05.28
Function TfrmTDenpyou.DeleteDenpyouDetail : Boolean;
var
	sSql : String;
  iID : Integer;
begin
	if (lbDenpyouCode.Caption = '') then begin
  	ShowMessage('伝票番号が特定できないため削除できません');
    Exit;
  end;

	sSql := 'DELETE FROM ' + CtblTDenpyouDetail;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' DenpyouCode =  ''' + lbDenpyouCode.Caption + '''';

  try
	  with frmDMMaster.QueryDelete do begin
  	  Close;
			Sql.Clear;
	    Sql.Add(sSql);
			ExecSql;
	  	Close;
	  end;//of with
    Result := True;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
	    Result := False;
    end;
  end;
end;



//Memoを表示する
procedure TfrmTDenpyou.CBUriageKingakuExit(Sender: TObject);
var
	sSql : String;
begin
//  inherited;
	//入力チェック
  if LabelID.Caption<>'ID' then begin
    exit;
  end;
  if CBUriageKingaku.Text = '' then exit;

	sSql := 'SELECT bikou FROM ' + CtblTDenpyou;
//	sSql := 'SELECT DenpyouCode FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 = ' + CBTokuisakiCode1.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate = ''' + DateToStr(DTP1.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' UriageKingaku = ' + CBUriageKingaku.Text;

	with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
   	EditMemo.Items.Clear;
    while not EOF do begin

    	EditMemo.Items.Add(FieldByName('bikou').AsString);
      //EditMemo.Items.Add(FieldByName('DenpyouCode').AsString);

      Next;
    end;//of while
    Close;
  end;//of with
  EditMemo.DroppedDown := True;
end;

//エクセルに出力ボタンがクリックされた
//1999/04/05
procedure TfrmTDenpyou.BitBtn2Click(Sender: TObject);
var
	sSql, sLine, sDate, sMemo : String;
 	F : TextFile;
begin

	sDate := DateToStr(DTP1.Date);
	//確認メッセージ
  if MessageDlg(sDate + '日の売上伝票を出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  sSql := 'SELECT ';
  sSql := sSql + 'D.DDate, ';
  sSql := sSql + 'D.TokuisakiCode1, ';
  sSql := sSql + 'T.TokuisakiName, ';
  sSql := sSql + 'D.UriageKingaku, ';
  sSql := sSql + 'D.Memo';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou   + ' D,';
  sSql := sSql + CtblMTokuisaki + ' T ';
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'DDate = ''' + sDate + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + 'D.TokuisakiCode1 = T.TokuisakiCode1';
  sSql := sSql + ' ORDER BY D.TokuisakiCode1';

	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_UriageDenpyou);
	Rewrite(F);
	CloseFile(F);

  //ファイルへ書き込み
  with frmDMMaster.QueryDenpyou do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	sLine := FieldByName('DDate').AsString;
    	sLine := sLine + ',' + FieldByName('TokuisakiCode1').AsString;
    	sLine := sLine + ',' + FieldByName('TokuisakiName').AsString;
    	sLine := sLine + ',' + FieldByName('UriageKingaku').AsString;

    	sMemo := Trim(FieldByName('Memo').AsString);
      if sMemo = '' then begin
      	sMemo := ' ';
      end else begin
	      sMemo := HKLib.CutStr(sMemo, '#10#13');
      end;

    	sLine := sLine + ',' + sMemo;

      if Trim(sLine) <> '' then begin
	      SB1.SimpleText := sLine;
  	    SB1.Update;
			  HMakeFile(CFileName_UriageDenpyou, sLine);
      end;//of if

      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '売上伝票一覧.xls', '', SW_SHOW);

end;

procedure TfrmTDenpyou.FormActivate(Sender: TObject);
begin
	if GPassWord = CPassword0 then begin
    Beep;
  end else begin
  	//ShowMessage('PassWordが違います');
    close;
  end;
  GPassWord := '';

  CBTokuisakiCode1.Setfocus;

end;

//訂正ボタンがクリックされた
//1999/06/20
procedure TfrmTDenpyou.BitBtn5Click(Sender: TObject);
var
	sSql,sTokuisakiCode1,sDDate,sKingaku : String;
  bFlag : Boolean;
begin

  if MessageDlg('訂正しますか？(旧伝票のみ)',mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
	 	Exit;
  end;
	if (LabelID.Caption = 'ID') or (LabelID.Caption = '0') then begin
  	ShowMessage('伝票番号が特定できないため削除できません');
    Exit;
  end;

  //まず削除
  if DeleteDenpyou = True then begin
		if InsertDenpyou = True then begin
		 	ShowMessage('訂正に成功しました');
  	end else begin
	  	ShowMessage('訂正に失敗しました');
    end;
  end else begin
  	ShowMessage('訂正に失敗しました');
  end;

end;

procedure TfrmTDenpyou.EditMemoExit(Sender: TObject);
begin
  lbDenpyouCode.Caption := '';
  LabelID.Caption       := 'ID';

  if trim(CBUriageKingaku.Text)='' then begin
    exit;
  end;
  //伝票のIDを取得
  lbDenpyouCode.Caption := GetID2();
  if lbDenpyouCode.Caption = '' then begin //旧伝票
	  LabelID.Caption := IntToStr(GetID);
  end;
  exit;
  if (LabelID.Caption = 'ID') or (LabelID.Caption = '0') then begin
	  LabelID.Caption := IntToStr(GetID);
  end;
  if LabelID.Caption='0' then begin
	  lbDenpyouCode.Caption := GetID2;
    LabelID.Caption := 'ID';
  end;

end;

procedure TfrmTDenpyou.EditMemoClick(Sender: TObject);
begin
  //伝票のIDを取得
  lbDenpyouCode.Caption := '';
  LabelID.Caption       := 'ID';

  lbDenpyouCode.Caption := GetID2();
  if lbDenpyouCode.Caption = '' then begin //旧伝票
   if (LabelID.Caption = 'ID') or (LabelID.Caption = '0') then begin
	  LabelID.Caption := IntToStr(GetID);
   end;
  end;
end;

end.
