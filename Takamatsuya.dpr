 program Takamatsuya;

uses
  Forms,
  main in 'main.pas' {frmMain},
  Master in 'Master.pas' {frmMaster},
  TDenpyou in 'TDenpyou.pas' {frmTDenpyou},
  TKaishuu in 'TKaishuu.pas' {frmTKaishuu},
  PBill in 'PBill.pas' {frmPBill},
  PEstimate in 'PEstimate.pas' {frmPEstimate},
  PUrikakeDaichou in 'PUrikakeDaichou.pas' {frmPUrikakeDaichou},
  DMMaster in 'DMMaster.pas' {frmDMMaster: TDataModule},
  Inter in 'Inter.pas',
  Hklib in 'Hklib.pas',
  PCourceUriage2 in 'PCourceUriage2.pas' {frmPCourceUriage2},
  PCourceUriage in 'PCourceUriage.pas' {frmPCourceUriage},
  PMishuukin in 'PMishuukin.pas' {frmPMishuukin},
  MTokuisaki2 in 'MTokuisaki2.pas' {frmMTokuisaki2},
  UrikakeZan in 'UrikakeZan.pas' {frmUrikakeZan},
  UriageShuukei in 'UriageShuukei.pas' {frmUriageShuukei},
  MGyoushu in 'MGyoushu.pas' {frmMGyoushu},
  DM1 in 'DM1.pas' {frmDM1: TDataModule},
  PUrikakeDaichou2 in 'PUrikakeDaichou2.pas' {frmPUrikakeDaichou2},
  Denpyou in 'Denpyou.pas' {frmDenpyou},
  TokuisakiKensaku in 'TokuisakiKensaku.pas' {frmTokuisakiKensaku},
  ItemKensaku in 'ItemKensaku.pas' {frmItemKensaku},
  MShiiresaki in 'MShiiresaki.pas' {frmMShiiresaki},
  MItem in 'MItem.pas' {frmMItem},
  MItemT in 'MItemT.pas' {frmMItemT},
  MItemC in 'MItemC.pas' {frmMItemC},
  Shukko in 'Shukko.pas' {frmShukko},
  ZaikoKanri in 'ZaikoKanri.pas' {frmZaikoKanri},
  Hacchuu in 'Hacchuu.pas' {frmHacchuu},
  Hacchuu2 in 'Hacchuu2.pas' {frmHacchuu2},
  Denpyou2 in 'Denpyou2.pas' {frmDenpyou2},
  CopyItem in 'CopyItem.pas' {frmCopyItem},
  DenpyouPrint in 'DenpyouPrint.pas' {frmDenpyouPrint},
  UriageDenpyouIkkatuInnsatu in 'UriageDenpyouIkkatuInnsatu.pas' {frmUriageDenpyouIkkatuInnsatu},
  GennkinnKaishuu in 'GennkinnKaishuu.pas' {frmGennkinnKaishuu},
  GennkinnZanndaka in 'GennkinnZanndaka.pas' {frmGennkinnZanndaka},
  DenpyouPrintGenkin in 'DenpyouPrintGenkin.pas' {frmDenpyouPrintGenkin},
  DenpyouPrintSeikyuu in 'DenpyouPrintSeikyuu.pas' {frmDenpyouPrintSeikyuu},
  CheckItem2 in 'CheckItem2.pas' {frmCheckItem2},
  PasswordDlg in 'PasswordDlg.pas' {dlgPasswordDlg},
  TokuisakibetsuKakaku in 'TokuisakibetsuKakaku.pas' {frmTokuisakibetsuKakaku},
  JyuchuuHindo in 'JyuchuuHindo.pas' {frmJyuchuuHindo},
  Juchuuhindo2 in 'Juchuuhindo2.pas' {frmJuchuuhindo2},
  IkkatsuHenkou4 in 'IkkatsuHenkou4.pas' {frmIkkatsuHenkou4},
  Sync_Hindo in 'Sync_Hindo.pas' {frmSync_Hindo},
  Nyuuko in 'Nyuuko.pas' {frmNyuuko},
  ShiireDaichou in 'ShiireDaichou.pas' {frmShiireDaichou},
  ShiharaiNyuuryoku in 'ShiharaiNyuuryoku.pas' {frmShiharaiNyuuryoku},
  Nyuuko2 in 'Nyuuko2.pas' {frmNyuuko1},
  Shime in 'Shime.pas' {frmShime},
  KaChouhyou in 'KaChouhyou.pas' {frmKaChouhyou},
  ShiireKakakuRireki in 'ShiireKakakuRireki.pas' {frmShiireKakakuRireki},
  MitsumoriKizon in 'MitsumoriKizon.pas' {frmMitsumoriKizon},
  Mitsumori in 'Mitsumori.pas' {frmMitsumori},
  MitsumoriArari in 'MitsumoriArari.pas' {frmMitsumoriArari},
  ChangeItem in 'ChangeItem.pas' {frmChangeItem},
  ListInfoMart in 'ListInfoMart.pas' {frmListInfoMart},
  InfoMart in 'InfoMart.pas' {frmInfoMart},
  AggregateSales in 'AggregateSales.pas' {frmAggregateSales},
  KakakuKoushin in 'KakakuKoushin.pas' {frmKaka-kuKoushin};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Takamatsuya2';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmDMMaster, frmDMMaster);
  Application.CreateForm(TfrmDM1, frmDM1);
  Application.CreateForm(TdlgPasswordDlg, dlgPasswordDlg);
  //  Application.CreateForm(TfrmPCourceUriage2, frmPCourceUriage2);
  //Application.CreateForm(TfrmKakakuKoushin, frmKakakuKoushin);
  //Application.CreateForm(TfrmAggregateSales, frmAggregateSales);
  //Application.CreateForm(TfrmAggregateSales, frmAggregateSales);
  //Application.CreateForm(TfrmAggregateSales, frmAggregateSales);
  //Application.CreateForm(TfrmInfoMart, frmInfoMart);
  //Application.CreateForm(TfrmListInfoMart, frmListInfoMart);
  //Application.CreateForm(TfrmInfoMart, frmInfoMart);
  //Application.CreateForm(TfrmListInfoMart, frmListInfoMart);
  //Application.CreateForm(TfrmChangeItem, frmChangeItem);
  //Application.CreateForm(TfrmMitsumori, frmMitsumori);
  //Application.CreateForm(TfrmMitsumoriArari, frmMitsumoriArari);
  Application.Run;

end.


