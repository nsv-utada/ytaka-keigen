inherited frmUriageShuukei: TfrmUriageShuukei
  Left = 541
  Top = 134
  Width = 555
  Height = 219
  Caption = 'frmUriageShuukei'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 539
    inherited Label1: TLabel
      Width = 120
      Caption = #22770#19978#38598#35336#30011#38754
    end
  end
  inherited Panel2: TPanel
    Top = 121
    Width = 539
    inherited BitBtn2: TBitBtn
      OnClick = BitBtn2Click
    end
  end
  inherited Panel3: TPanel
    Width = 539
    Height = 80
    object Label3: TLabel
      Left = 16
      Top = 10
      Width = 252
      Height = 13
      Caption = #22770#19978#12370#12434#38598#35336#12377#12427#26399#38291#12434#25351#23450#12375#12390#12367#12384#12373#12356#12290
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 214
      Top = 50
      Width = 25
      Height = 13
      Caption = #12363#12425
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 446
      Top = 50
      Width = 26
      Height = 13
      Caption = #12414#12391
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 342
      Top = 48
      Width = 14
      Height = 13
      Caption = #24180
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 416
      Top = 50
      Width = 14
      Height = 13
      Caption = #26376
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 110
      Top = 46
      Width = 14
      Height = 13
      Caption = #24180
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 180
      Top = 48
      Width = 14
      Height = 13
      Caption = #26376
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CBYyyyTo: TComboBox
      Left = 268
      Top = 46
      Width = 71
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      Text = '2004'
      Items.Strings = (
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020')
    end
    object CBMmTo: TComboBox
      Left = 368
      Top = 46
      Width = 43
      Height = 21
      DropDownCount = 12
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
      Text = '3'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
    end
    object CBYyyyFrom: TComboBox
      Left = 36
      Top = 44
      Width = 71
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 2
      Text = '2004'
      Items.Strings = (
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020')
    end
    object CBMmFrom: TComboBox
      Left = 134
      Top = 44
      Width = 43
      Height = 21
      DropDownCount = 12
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 3
      Text = '1'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
    end
  end
  inherited SB1: TStatusBar
    Top = 162
    Width = 539
  end
  object QueryTokuisaki: TQuery
    DatabaseName = 'taka'
    Left = 232
    Top = 1
  end
  object QueryCalc: TQuery
    DatabaseName = 'taka'
    Left = 196
  end
end
